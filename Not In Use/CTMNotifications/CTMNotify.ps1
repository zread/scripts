﻿$threshold = 99.00
$SQLServer = "CA01A0047"
$user = "reader"
$password = "12345"
$SQLDBName = "CAPA01DB01"
$TodayDate = Get-Date
$connectionString = "Server=$dataSource;uid=$user; pwd=$password;Database=$database;Integrated Security=False;"
$SqlQuery = 
"Select * From(
	Select Line, Shifts, CTM, Timetaken, Row_Number() Over(Partition By Line Order By Timetaken DESC) rn 
	From CTMStatsDaily Where Cast(GetDate() as Date) = Cast(Timetaken as Date)
) tb1 where rn in (1,2) order by Line"

$smtpServer = "csica01ms01.csisolar.com"
$msg = new-object Net.Mail.MailMessage
$smtp = new-object Net.Mail.SmtpClient($smtpServer)
$msg.From = "GuelphProduction.Err@canadiansolar.com"
#$msg.ReplyTo = "replyto@xxxx.com"
$msg.To.Add("Zach.Read@canadiansolar.com")
#$msg.To.Add("CAMO_Maintenance@canadiansolar.com")
$msg.To.Add("CAMO_Engineering@canadiansolar.com")



$SqlConnection = New-Object System.Data.SqlClient.SqlConnection
$SqlConnection.ConnectionString = "Server = $SQLServer; Database = $SQLDBName; Integrated Security = True"
 
$SqlCmd = New-Object System.Data.SqlClient.SqlCommand
$SqlCmd.CommandText = $SqlQuery
$SqlCmd.Connection = $SqlConnection
 
$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
$SqlAdapter.SelectCommand = $SqlCmd
 
$DataSet = New-Object System.Data.DataSet
$SqlAdapter.Fill($DataSet)
 
$SqlConnection.Close()

$Line = ""

$strMessage = "The following line has a CTM below 90 percent.`n" 
$messageNeeded = 0 

for($i=0;$i -le $DataSet.Tables[0].Rows.Count - 1 ;$i = $i + 2)
{ 
    if($threshold -gt $DataSet.Tables[0].Rows[$i][2])
    {
        if($DataSet.Tables[0].Rows[$i+1][2] -gt $threshold -and $DataSet.Tables[0].Rows[$i+1][0] -eq $DataSet.Tables[0].Rows[$i][0])
        {
            $Line = $Line + $DataSet.Tables[0].Rows[$i][0] + ","
            $strMessage = "" + $strMessage + "Line " + $DataSet.Tables[0].Rows[$i][0] + "`n"
            $messageNeeded = 1
        }
        if(($TodayDate.Hour -eq 8 -and $TodayDate.Minute -lt 40) -and $messageNeeded -eq 0)
        {
            $Line = $Line + $DataSet.Tables[0].Rows[$i][0] + ","
            $strMessage = "" + $strMessage + "Line " + $DataSet.Tables[0].Rows[$i][0] + "`n"
            $messageNeeded = 1
        }
    }
}

if($messageNeeded -eq 1)
{    
    $msg.body = "Hello, this is an automated message. `n `n" + $strMessage
	$msg.subject = "Warning: Line " + $Line + " CTM is below 90 Percent"	
    $smtp.Send($msg)
}

Start-Sleep -s 2
Remove-Variable strMessage
Remove-Variable Line

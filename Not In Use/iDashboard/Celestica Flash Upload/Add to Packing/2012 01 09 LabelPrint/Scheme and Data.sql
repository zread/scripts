USE [CSILabelPrintDB]
GO
/****** Object:  Table [dbo].[UserLogin]    Script Date: 05/29/2011 21:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserLogin]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserLogin](
	[InterID] [int] NOT NULL,
	[UserNM] [nvarchar](50) NOT NULL,
	[UserPW] [nvarchar](50) NOT NULL,
	[UserNote] [nvarchar](200) NULL,
	[CreateDate] [datetime] NULL,
	[CreateUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
	[Status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[InterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
INSERT [dbo].[UserLogin] ([InterID], [UserNM], [UserPW], [UserNote], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser], [Status]) VALUES (0, N'admin', N'admin', N'administrator', CAST(0x00009ED400FCAF80 AS DateTime), 0, CAST(0x00009ED400FCC6F0 AS DateTime), 0, 1)
INSERT [dbo].[UserLogin] ([InterID], [UserNM], [UserPW], [UserNote], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser], [Status]) VALUES (3, N'Test', N'test', N'Tester', CAST(0x00009EEF001C5390 AS DateTime), 0, CAST(0x00009EEF00353BBC AS DateTime), 0, 1)
INSERT [dbo].[UserLogin] ([InterID], [UserNM], [UserPW], [UserNote], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser], [Status]) VALUES (4, N'dxl', N'123', N'dxl', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[UserLogin] ([InterID], [UserNM], [UserPW], [UserNote], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser], [Status]) VALUES (5, N'tester', N'1234', N'tester', CAST(0x00009EF2015C951C AS DateTime), 0, NULL, NULL, 1)
/****** Object:  Table [dbo].[SchemeEntry]    Script Date: 05/29/2011 21:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SchemeEntry]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SchemeEntry](
	[PmaxID] [nvarchar](50) NULL,
	[PmaxNM] [nvarchar](50) NULL,
	[PmaxNum] [int] NOT NULL,
	[Pmax] [nvarchar](50) NULL,
	[MinValue] [nvarchar](100) NULL,
	[CtlValue] [nvarchar](50) NULL,
	[MaxValue] [nvarchar](100) NULL,
	[CreateDate] [datetime] NULL,
	[CreateUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUser] [int] NULL
) ON [PRIMARY]
END
GO
INSERT [dbo].[SchemeEntry] ([PmaxID], [PmaxNM], [PmaxNum], [Pmax], [MinValue], [CtlValue], [MaxValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (N'A', N'A', 1, N'230W', N'[Measured power]>= 225', N'And', N'[Measured power]<= 235', NULL, NULL, NULL, NULL)
INSERT [dbo].[SchemeEntry] ([PmaxID], [PmaxNM], [PmaxNum], [Pmax], [MinValue], [CtlValue], [MaxValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (N'B', N'B', 1, N'180W', N'[Measured power]> 176.5', N'And', N'[Measured power]< 183.2', NULL, NULL, NULL, NULL)
INSERT [dbo].[SchemeEntry] ([PmaxID], [PmaxNM], [PmaxNum], [Pmax], [MinValue], [CtlValue], [MaxValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (N'A', N'A', 2, N'220W', N'[Measured power]= 223', N'', N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[SchemeEntry] ([PmaxID], [PmaxNM], [PmaxNum], [Pmax], [MinValue], [CtlValue], [MaxValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (N'C', N'C', 1, N'180W', N'[Measured power]>= 178', N'And', N'[Measured power]<= 195', NULL, NULL, NULL, NULL)
/****** Object:  Table [dbo].[Scheme]    Script Date: 05/29/2011 21:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Scheme]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Scheme](
	[InterID] [int] NOT NULL,
	[SchemeID] [nvarchar](50) NOT NULL,
	[SchemeNM] [nvarchar](50) NOT NULL,
	[ItemMPID] [int] NULL,
	[ItemColorID] [int] NULL,
	[ItemSizeID] [int] NULL,
	[ItemPointID] [int] NULL,
	[ItemLogoID] [int] NULL,
	[LogoQty] [int] NULL,
	[ItemLableID] [int] NULL,
	[LableQty] [int] NULL,
	[ItemModelID] [int] NULL,
	[ItemPmaxID] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[CreateUserID] [int] NULL,
	[ModifyUserID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[InterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
INSERT [dbo].[Scheme] ([InterID], [SchemeID], [SchemeNM], [ItemMPID], [ItemColorID], [ItemSizeID], [ItemPointID], [ItemLogoID], [LogoQty], [ItemLableID], [LableQty], [ItemModelID], [ItemPmaxID], [CreateDate], [ModifyDate], [CreateUserID], [ModifyUserID]) VALUES (1, N'a', N'CS5A-P', 12, 19, 21, 1, 17, 1, 15, 1, 1, N'A', CAST(0x00009EEF0041562C AS DateTime), NULL, 0, NULL)
INSERT [dbo].[Scheme] ([InterID], [SchemeID], [SchemeNM], [ItemMPID], [ItemColorID], [ItemSizeID], [ItemPointID], [ItemLogoID], [LogoQty], [ItemLableID], [LableQty], [ItemModelID], [ItemPmaxID], [CreateDate], [ModifyDate], [CreateUserID], [ModifyUserID]) VALUES (2, N'b', N'CS6A-M', 13, 20, 22, 1, 18, 1, 16, 1, 4, N'B', CAST(0x00009EEF004190C4 AS DateTime), NULL, 0, NULL)
INSERT [dbo].[Scheme] ([InterID], [SchemeID], [SchemeNM], [ItemMPID], [ItemColorID], [ItemSizeID], [ItemPointID], [ItemLogoID], [LogoQty], [ItemLableID], [LableQty], [ItemModelID], [ItemPmaxID], [CreateDate], [ModifyDate], [CreateUserID], [ModifyUserID]) VALUES (3, N'c', N'CS5B-PE', 14, 19, 22, 1, 18, 1, 16, 1, 2, N'A', CAST(0x00009EEF00459B88 AS DateTime), NULL, 0, NULL)
INSERT [dbo].[Scheme] ([InterID], [SchemeID], [SchemeNM], [ItemMPID], [ItemColorID], [ItemSizeID], [ItemPointID], [ItemLogoID], [LogoQty], [ItemLableID], [LableQty], [ItemModelID], [ItemPmaxID], [CreateDate], [ModifyDate], [CreateUserID], [ModifyUserID]) VALUES (4, N'd', N'CS6A-PS', 24, 19, 22, 1, 18, 1, 16, 1, 4, N'A', CAST(0x00009EF2015DFCE0 AS DateTime), CAST(0x00009EF2015E2260 AS DateTime), 0, 0)
/****** Object:  Table [dbo].[Running]    Script Date: 05/29/2011 21:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Running]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Running](
	[InterID] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[SchemeInterID] [int] NOT NULL,
	[RunningUserID] [int] NULL,
	[RunningDate] [datetime] NULL,
	[FinishUserID] [int] NULL,
	[FinishDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[InterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[RoleItem]    Script Date: 05/29/2011 21:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RoleItem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RoleItem](
	[InterID] [int] NOT NULL,
	[UserInterID] [int] NOT NULL,
	[FuncGroupInterID] [int] NOT NULL,
	[FuncItemInterID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[InterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
INSERT [dbo].[RoleItem] ([InterID], [UserInterID], [FuncGroupInterID], [FuncItemInterID]) VALUES (1, 3, 1, 0)
INSERT [dbo].[RoleItem] ([InterID], [UserInterID], [FuncGroupInterID], [FuncItemInterID]) VALUES (2, 3, 2, 1)
INSERT [dbo].[RoleItem] ([InterID], [UserInterID], [FuncGroupInterID], [FuncItemInterID]) VALUES (3, 3, 3, 2)
INSERT [dbo].[RoleItem] ([InterID], [UserInterID], [FuncGroupInterID], [FuncItemInterID]) VALUES (4, 3, 3, 3)
INSERT [dbo].[RoleItem] ([InterID], [UserInterID], [FuncGroupInterID], [FuncItemInterID]) VALUES (5, 3, 4, 4)
INSERT [dbo].[RoleItem] ([InterID], [UserInterID], [FuncGroupInterID], [FuncItemInterID]) VALUES (6, 3, 5, 5)
INSERT [dbo].[RoleItem] ([InterID], [UserInterID], [FuncGroupInterID], [FuncItemInterID]) VALUES (7, 3, 6, 6)
INSERT [dbo].[RoleItem] ([InterID], [UserInterID], [FuncGroupInterID], [FuncItemInterID]) VALUES (8, 3, 7, 7)
INSERT [dbo].[RoleItem] ([InterID], [UserInterID], [FuncGroupInterID], [FuncItemInterID]) VALUES (9, 3, 7, 8)
INSERT [dbo].[RoleItem] ([InterID], [UserInterID], [FuncGroupInterID], [FuncItemInterID]) VALUES (10, 3, 8, 9)
INSERT [dbo].[RoleItem] ([InterID], [UserInterID], [FuncGroupInterID], [FuncItemInterID]) VALUES (11, 3, 8, 11)
/****** Object:  Table [dbo].[Product]    Script Date: 05/29/2011 21:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Product]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Product](
	[SN] [nvarchar](20) NOT NULL,
	[ModelType] [nvarchar](10) NULL,
	[CellSpec] [nvarchar](10) NULL,
	[CellType] [nvarchar](10) NULL,
	[CellDegree] [nvarchar](10) NULL,
	[CellProvider] [nvarchar](10) NULL,
	[CellColor] [nvarchar](10) NULL,
	[Process] [nvarchar](2) NULL,
	[ProcessPass] [bit] NULL,
	[ModelTypeLabel] [varchar](10) NULL,
	[Degree] [varchar](1) NULL,
	[BoxID] [nvarchar](10) NULL,
	[Mix] [nvarchar](8) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430020001', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T5', NULL, NULL, NULL, N'011106700B', N'')
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430020002', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T5', NULL, NULL, NULL, N'011106700B', N'')
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430020081', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T5', NULL, NULL, NULL, N'011106700B', N'')
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430020082', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T5', NULL, NULL, NULL, N'011106701B', N'')
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430020083', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430020084', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430020089', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430020085', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430020086', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430020087', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430020088', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430020018', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430020090', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430020011', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430020012', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430020028', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430020038', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430020048', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430020558', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430021001', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430021002', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430021081', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430021082', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430021083', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430021084', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430021089', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430021085', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430021086', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430021087', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430021088', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430021018', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430021090', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430021011', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430021012', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430021028', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430021038', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430021048', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0710430021558', N'CS5A-P', N'125*125', N'P', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0810430021001', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T5', NULL, NULL, NULL, N'011106701B', N'')
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0810430021002', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0810430021081', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0810430021082', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0810430021083', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0810430021084', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0810430021089', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0810430021085', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0810430021086', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0810430021087', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0810430021088', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0810430021018', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0810430021090', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0810430021011', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0810430021012', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0810430021028', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0810430021038', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0810430021048', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'0810430021558', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'8710430020001', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'8710430020002', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'8710430020081', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'8710430020082', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'8710430020083', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'8710430020084', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'8710430020089', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'8710430020085', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'8710430020086', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'8710430020087', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'8710430020088', N'CS6A-M', N'156*156', N'M', NULL, N'/', N'Dark', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'8710430020018', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'8710430020090', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T5', NULL, NULL, NULL, N'011106701B', N'')
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'8710430020011', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'8710430020012', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'8710430020028', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'8710430020038', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'8710430020048', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'8710430020558', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2810430021001', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2810430021002', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2810430021081', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2810430021082', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2810430021083', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2810430021084', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2810430021089', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2810430021085', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2810430021086', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2810430021087', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2810430021088', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2810430021018', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2810430021090', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2810430021011', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2810430021012', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2810430021028', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2810430021038', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2810430021048', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2810430021558', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2710430020001', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2710430020002', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2710430020081', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2710430020082', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2710430020083', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2710430020084', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
GO
print 'Processed 100 total records'
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2710430020089', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2710430020085', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2710430020086', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2710430020087', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2710430020088', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2710430020018', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2710430020090', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2710430020011', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2710430020012', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2710430020028', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2710430020038', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2710430020048', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
INSERT [dbo].[Product] ([SN], [ModelType], [CellSpec], [CellType], [CellDegree], [CellProvider], [CellColor], [Process], [ProcessPass], [ModelTypeLabel], [Degree], [BoxID], [Mix]) VALUES (N'2710430020558', N'CS5B-PE', N'156*156', N'PE', NULL, N'/', N'Blue', N'T4', NULL, NULL, NULL, N'', NULL)
/****** Object:  Table [dbo].[Process]    Script Date: 05/29/2011 21:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Process]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Process](
	[ProcessID] [bigint] IDENTITY(1,1) NOT NULL,
	[SN] [nvarchar](20) NOT NULL,
	[OperatorID] [nvarchar](20) NULL,
	[ProcessDate] [datetime] NULL,
	[Process] [nvarchar](2) NULL,
	[Tester] [nvarchar](10) NULL,
	[Pass] [bit] NULL,
	[UPReason] [nvarchar](20) NULL,
	[Mix] [nvarchar](8) NULL
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[Process] ON
INSERT [dbo].[Process] ([ProcessID], [SN], [OperatorID], [ProcessDate], [Process], [Tester], [Pass], [UPReason], [Mix]) VALUES (1, N'0710430020081', N'admin', CAST(0x00009EEF017F040B AS DateTime), N'T5', N'', 1, N'', N'')
INSERT [dbo].[Process] ([ProcessID], [SN], [OperatorID], [ProcessDate], [Process], [Tester], [Pass], [UPReason], [Mix]) VALUES (2, N'0710430020002', N'admin', CAST(0x00009EEF017F040B AS DateTime), N'T5', N'', 1, N'', N'')
INSERT [dbo].[Process] ([ProcessID], [SN], [OperatorID], [ProcessDate], [Process], [Tester], [Pass], [UPReason], [Mix]) VALUES (3, N'0710430020001', N'admin', CAST(0x00009EEF017F0410 AS DateTime), N'T5', N'', 1, N'', N'')
INSERT [dbo].[Process] ([ProcessID], [SN], [OperatorID], [ProcessDate], [Process], [Tester], [Pass], [UPReason], [Mix]) VALUES (4, N'8710430020090', N'admin', CAST(0x00009EF0002D4478 AS DateTime), N'T5', N'', 1, N'', N'')
INSERT [dbo].[Process] ([ProcessID], [SN], [OperatorID], [ProcessDate], [Process], [Tester], [Pass], [UPReason], [Mix]) VALUES (5, N'0810430021001', N'admin', CAST(0x00009EF0002D447D AS DateTime), N'T5', N'', 1, N'', N'')
INSERT [dbo].[Process] ([ProcessID], [SN], [OperatorID], [ProcessDate], [Process], [Tester], [Pass], [UPReason], [Mix]) VALUES (6, N'0710430020082', N'admin', CAST(0x00009EF0002D447D AS DateTime), N'T5', N'', 1, N'', N'')
SET IDENTITY_INSERT [dbo].[Process] OFF
/****** Object:  Table [dbo].[Point]    Script Date: 05/29/2011 21:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Point]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Point](
	[InterID] [int] NOT NULL,
	[PointID] [nvarchar](50) NOT NULL,
	[Pmpp] [int] NOT NULL,
	[Vmpp] [int] NOT NULL,
	[Impp] [int] NOT NULL,
	[Voc] [int] NOT NULL,
	[Isc] [int] NOT NULL,
	[CreateDate] [datetime] NULL,
	[CreateUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[InterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
INSERT [dbo].[Point] ([InterID], [PointID], [Pmpp], [Vmpp], [Impp], [Voc], [Isc], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (1, N'a', 1, 2, 2, 2, 2, CAST(0x00009EEF0040F740 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Point] ([InterID], [PointID], [Pmpp], [Vmpp], [Impp], [Voc], [Isc], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (2, N'b', 2, 2, 2, 2, 2, CAST(0x00009EF2015D6DE8 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Point] ([InterID], [PointID], [Pmpp], [Vmpp], [Impp], [Voc], [Isc], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (3, N'c', 1, 2, 2, 2, 2, CAST(0x00009EF2015D7874 AS DateTime), 0, NULL, NULL)
/****** Object:  Table [dbo].[NewTempTable]    Script Date: 05/29/2011 21:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NewTempTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[NewTempTable](
	[FNumber] [nvarchar](255) NULL,
	[Temp] [float] NULL,
	[VOC] [float] NULL,
	[ISC] [float] NULL,
	[Pmax] [float] NULL,
	[Vm] [float] NULL,
	[Im] [float] NULL,
	[FF] [float] NULL,
	[Eff] [float] NULL,
	[Serial] [float] NULL,
	[Shunt] [float] NULL,
	[Time] [nvarchar](50) NULL,
	[Date] [nvarchar](50) NULL,
	[Operator] [nvarchar](50) NULL,
	[SchemeInterID] [int] NULL,
	[CheckDate] [datetime] NULL,
	[FNO] [int] NULL
) ON [PRIMARY]
END
GO
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430020001', 23, 36.54, 8.56, 231.99, 29.25, 7.93, 0.74, 0.16, 0.54, 329.09, N'21:21:54', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430020002', 23, 36.53, 8.56, 231.74, 29.11, 7.96, 0.74, 0.16, 0.54, 318.72, N'21:22:25', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430020081', 23, 36.52, 8.55, 231.73, 29.11, 7.96, 0.74, 0.16, 0.54, 472.75, N'21:23:22', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430020082', 23, 36.53, 8.54, 231.88, 29.11, 7.97, 0.74, 0.16, 0.54, 513.29, N'21:24:27', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430020083', 23, 36.52, 8.55, 231.88, 29.11, 7.97, 0.74, 0.16, 0.53, 439.43, N'21:25:06', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430020084', 23, 36.53, 8.55, 231.74, 29.11, 7.96, 0.74, 0.16, 0.54, 418.82, N'21:26:11', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430020089', 23, 36.52, 8.54, 231.45, 29.11, 7.95, 0.74, 0.16, 0.53, 525.63, N'21:27:27', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430020085', 23, 36.53, 8.55, 231.74, 29.11, 7.96, 0.74, 0.16, 0.54, 374.58, N'21:28:19', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430020086', 23, 36.53, 8.55, 231.74, 29.11, 7.96, 74.19, 0.16, 0.54, 371.19, N'21:29:03', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430020087', 23, 36.53, 8.55, 231.73, 29.11, 7.96, 74.2, 15.87, 0.53, 407.15, N'21:29:35', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430020088', 23, 36.52, 8.55, 231.88, 29.11, 7.97, 74.28, 15.88, 0.54, 457.1, N'21:30:06', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430020018', 23, 36.52, 8.55, 231.59, 29.11, 7.96, 74.19, 15.86, 0.53, 480.73, N'21:30:38', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430020090', 23, 36.53, 8.55, 231.85, 29.16, 7.95, 74.26, 15.88, 0.54, 442.67, N'21:31:09', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430020011', 23, 36.54, 8.56, 231.99, 29.25, 7.93, 0.74, 0.16, 0.54, 329.09, N'21:21:54', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430020012', 23, 36.53, 8.56, 231.74, 29.11, 7.96, 0.74, 0.16, 0.54, 318.72, N'21:22:25', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430020028', 23, 36.52, 8.55, 231.73, 29.11, 7.96, 0.74, 0.16, 0.54, 472.75, N'21:23:22', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430020038', 23, 36.53, 8.54, 231.88, 29.11, 7.97, 0.74, 0.16, 0.54, 513.29, N'21:24:27', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430020048', 23, 36.52, 8.55, 231.88, 29.11, 7.97, 0.74, 0.16, 0.53, 439.43, N'21:25:06', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430020558', 23, 36.53, 8.55, 231.74, 29.11, 7.96, 0.74, 0.16, 0.54, 418.82, N'21:26:11', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430021001', 23, 36.54, 8.56, 231.99, 29.25, 7.93, 0.74, 0.16, 0.54, 329.09, N'21:21:54', N'2010-12-01', N'yongxing.tian----', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430021002', 23, 36.53, 8.56, 231.74, 29.11, 7.96, 0.74, 0.16, 0.54, 318.72, N'21:22:25', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430021081', 23, 36.52, 8.55, 231.73, 29.11, 7.96, 0.74, 0.16, 0.54, 472.75, N'21:23:22', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430021082', 23, 36.53, 8.54, 231.88, 29.11, 7.97, 0.74, 0.16, 0.54, 513.29, N'21:24:27', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430021083', 23, 36.52, 8.55, 231.88, 29.11, 7.97, 0.74, 0.16, 0.53, 439.43, N'21:25:06', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430021084', 23, 36.53, 8.55, 231.74, 29.11, 7.96, 0.74, 0.16, 0.54, 418.82, N'21:26:11', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430021089', 23, 36.52, 8.54, 231.45, 29.11, 7.95, 0.74, 0.16, 0.53, 525.63, N'21:27:27', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430021085', 23, 36.53, 8.55, 231.74, 29.11, 7.96, 0.74, 0.16, 0.54, 374.58, N'21:28:19', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430021086', 23, 36.53, 8.55, 231.74, 29.11, 7.96, 74.19, 0.16, 0.54, 371.19, N'21:29:03', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430021087', 23, 36.53, 8.55, 231.73, 29.11, 7.96, 74.2, 15.87, 0.53, 407.15, N'21:29:35', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430021088', 23, 36.52, 8.55, 231.88, 29.11, 7.97, 74.28, 15.88, 0.54, 457.1, N'21:30:06', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430021018', 23, 36.52, 8.55, 231.59, 29.11, 7.96, 74.19, 15.86, 0.53, 480.73, N'21:30:38', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430021090', 23, 36.53, 8.55, 231.85, 29.16, 7.95, 74.26, 15.88, 0.54, 442.67, N'21:31:09', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430021011', 23, 36.54, 8.56, 231.99, 29.25, 7.93, 0.74, 0.16, 0.54, 329.09, N'21:21:54', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430021012', 23, 36.53, 8.56, 231.74, 29.11, 7.96, 0.74, 0.16, 0.54, 318.72, N'21:22:25', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430021028', 23, 36.52, 8.55, 231.73, 29.11, 7.96, 0.74, 0.16, 0.54, 472.75, N'21:23:22', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430021038', 23, 36.53, 8.54, 231.88, 29.11, 7.97, 0.74, 0.16, 0.54, 513.29, N'21:24:27', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430021048', 23, 36.52, 8.55, 231.88, 29.11, 7.97, 0.74, 0.16, 0.53, 439.43, N'21:25:06', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0710430021558', 23, 36.53, 8.55, 231.74, 29.11, 7.96, 0.74, 0.16, 0.54, 418.82, N'21:26:11', N'2010-12-01', N'yongxing.tian', 1, CAST(0x00009EEF0043F800 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0810430021001', 23, 36.54, 8.56, 181.99, 29.25, 7.93, 0.74, 0.16, 0.54, 329.09, N'21:21:54', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0810430021002', 23, 36.53, 8.56, 181.74, 29.11, 7.96, 0.74, 0.16, 0.54, 318.72, N'21:22:25', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0810430021081', 23, 36.52, 8.55, 181.73, 29.11, 7.96, 0.74, 0.16, 0.54, 472.75, N'21:23:22', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0810430021082', 23, 36.53, 8.54, 181.88, 29.11, 7.97, 0.74, 0.16, 0.54, 513.29, N'21:24:27', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0810430021083', 23, 36.52, 8.55, 181.88, 29.11, 7.97, 0.74, 0.16, 0.53, 439.43, N'21:25:06', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0810430021084', 23, 36.53, 8.55, 181.74, 29.11, 7.96, 0.74, 0.16, 0.54, 418.82, N'21:26:11', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0810430021089', 23, 36.52, 8.54, 181.45, 29.11, 7.95, 0.74, 0.16, 0.53, 525.63, N'21:27:27', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0810430021085', 23, 36.53, 8.55, 181.74, 29.11, 7.96, 0.74, 0.16, 0.54, 374.58, N'21:28:19', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0810430021086', 23, 36.53, 8.55, 181.74, 29.11, 7.96, 74.19, 0.16, 0.54, 371.19, N'21:29:03', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0810430021087', 23, 36.53, 8.55, 181.73, 29.11, 7.96, 74.2, 15.87, 0.53, 407.15, N'21:29:35', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0810430021088', 23, 36.52, 8.55, 181.88, 29.11, 7.97, 74.28, 15.88, 0.54, 457.1, N'21:30:06', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0810430021018', 23, 36.52, 8.55, 181.59, 29.11, 7.96, 74.19, 15.86, 0.53, 480.73, N'21:30:38', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0810430021090', 23, 36.53, 8.55, 181.85, 29.16, 7.95, 74.26, 15.88, 0.54, 442.67, N'21:31:09', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0810430021011', 23, 36.54, 8.56, 181.99, 29.25, 7.93, 0.74, 0.16, 0.54, 329.09, N'21:21:54', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0810430021012', 23, 36.53, 8.56, 181.74, 29.11, 7.96, 0.74, 0.16, 0.54, 318.72, N'21:22:25', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0810430021028', 23, 36.52, 8.55, 181.73, 29.11, 7.96, 0.74, 0.16, 0.54, 472.75, N'21:23:22', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0810430021038', 23, 36.53, 8.54, 181.88, 29.11, 7.97, 0.74, 0.16, 0.54, 513.29, N'21:24:27', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0810430021048', 23, 36.52, 8.55, 181.88, 29.11, 7.97, 0.74, 0.16, 0.53, 439.43, N'21:25:06', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'0810430021558', 23, 36.53, 8.55, 181.74, 29.11, 7.96, 0.74, 0.16, 0.54, 418.82, N'21:26:11', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'8710430020001', 23, 36.54, 8.56, 181.99, 29.25, 7.93, 0.74, 0.16, 0.54, 329.09, N'21:21:54', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'8710430020002', 23, 36.53, 8.56, 181.74, 29.11, 7.96, 0.74, 0.16, 0.54, 318.72, N'21:22:25', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'8710430020081', 23, 36.52, 8.55, 181.73, 29.11, 7.96, 0.74, 0.16, 0.54, 472.75, N'21:23:22', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'8710430020082', 23, 36.53, 8.54, 181.88, 29.11, 7.97, 0.74, 0.16, 0.54, 513.29, N'21:24:27', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'8710430020083', 23, 36.52, 8.55, 181.88, 29.11, 7.97, 0.74, 0.16, 0.53, 439.43, N'21:25:06', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'8710430020084', 23, 36.53, 8.55, 181.74, 29.11, 7.96, 0.74, 0.16, 0.54, 418.82, N'21:26:11', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'8710430020089', 23, 36.52, 8.54, 181.45, 29.11, 7.95, 0.74, 0.16, 0.53, 525.63, N'21:27:27', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'8710430020085', 23, 36.53, 8.55, 181.74, 29.11, 7.96, 0.74, 0.16, 0.54, 374.58, N'21:28:19', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'8710430020086', 23, 36.53, 8.55, 181.74, 29.11, 7.96, 74.19, 0.16, 0.54, 371.19, N'21:29:03', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'8710430020087', 23, 36.53, 8.55, 181.73, 29.11, 7.96, 74.2, 15.87, 0.53, 407.15, N'21:29:35', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'8710430020088', 23, 36.52, 8.55, 181.88, 29.11, 7.97, 74.28, 15.88, 0.54, 457.1, N'21:30:06', N'2010-12-01', N'yongxing.tian', 2, CAST(0x00009EEF00453A44 AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'8710430020018', 23, 36.52, 8.55, 231.59, 29.11, 7.96, 74.19, 15.86, 0.53, 480.73, N'21:30:38', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'8710430020090', 23, 36.53, 8.55, 231.85, 29.16, 7.95, 74.26, 15.88, 0.54, 442.67, N'21:31:09', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'8710430020011', 23, 36.54, 8.56, 231.99, 29.25, 7.93, 0.74, 0.16, 0.54, 329.09, N'21:21:54', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'8710430020012', 23, 36.53, 8.56, 231.74, 29.11, 7.96, 0.74, 0.16, 0.54, 318.72, N'21:22:25', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'8710430020028', 23, 36.52, 8.55, 231.73, 29.11, 7.96, 0.74, 0.16, 0.54, 472.75, N'21:23:22', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'8710430020038', 23, 36.53, 8.54, 231.88, 29.11, 7.97, 0.74, 0.16, 0.54, 513.29, N'21:24:27', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'8710430020048', 23, 36.52, 8.55, 231.88, 29.11, 7.97, 0.74, 0.16, 0.53, 439.43, N'21:25:06', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'8710430020558', 23, 36.53, 8.55, 231.74, 29.11, 7.96, 0.74, 0.16, 0.54, 418.82, N'21:26:11', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2810430021001', 23, 36.54, 8.56, 221.99, 29.25, 7.93, 0.74, 0.16, 0.54, 329.09, N'21:21:54', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2810430021002', 23, 36.53, 8.56, 221.74, 29.11, 7.96, 0.74, 0.16, 0.54, 318.72, N'21:22:25', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2810430021081', 23, 36.52, 8.55, 221.73, 29.11, 7.96, 0.74, 0.16, 0.54, 472.75, N'21:23:22', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2810430021082', 23, 36.53, 8.54, 221.88, 29.11, 7.97, 0.74, 0.16, 0.54, 513.29, N'21:24:27', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2810430021083', 23, 36.52, 8.55, 221.88, 29.11, 7.97, 0.74, 0.16, 0.53, 439.43, N'21:25:06', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2810430021084', 23, 36.53, 8.55, 221.74, 29.11, 7.96, 0.74, 0.16, 0.54, 418.82, N'21:26:11', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2810430021089', 23, 36.52, 8.54, 221.45, 29.11, 7.95, 0.74, 0.16, 0.53, 525.63, N'21:27:27', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2810430021085', 23, 36.53, 8.55, 221.74, 29.11, 7.96, 0.74, 0.16, 0.54, 374.58, N'21:28:19', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2810430021086', 23, 36.53, 8.55, 221.74, 29.11, 7.96, 74.19, 0.16, 0.54, 371.19, N'21:29:03', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2810430021087', 23, 36.53, 8.55, 221.73, 29.11, 7.96, 74.2, 15.87, 0.53, 407.15, N'21:29:35', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2810430021088', 23, 36.52, 8.55, 221.88, 29.11, 7.97, 74.28, 15.88, 0.54, 457.1, N'21:30:06', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2810430021018', 23, 36.52, 8.55, 221.59, 29.11, 7.96, 74.19, 15.86, 0.53, 480.73, N'21:30:38', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2810430021090', 23, 36.53, 8.55, 221.85, 29.16, 7.95, 74.26, 15.88, 0.54, 442.67, N'21:31:09', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2810430021011', 23, 36.54, 8.56, 221.99, 29.25, 7.93, 0.74, 0.16, 0.54, 329.09, N'21:21:54', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2810430021012', 23, 36.53, 8.56, 221.74, 29.11, 7.96, 0.74, 0.16, 0.54, 318.72, N'21:22:25', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2810430021028', 23, 36.52, 8.55, 221.73, 29.11, 7.96, 0.74, 0.16, 0.54, 472.75, N'21:23:22', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2810430021038', 23, 36.53, 8.54, 221.88, 29.11, 7.97, 0.74, 0.16, 0.54, 513.29, N'21:24:27', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2810430021048', 23, 36.52, 8.55, 221.88, 29.11, 7.97, 0.74, 0.16, 0.53, 439.43, N'21:25:06', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2810430021558', 23, 36.53, 8.55, 221.74, 29.11, 7.96, 0.74, 0.16, 0.54, 418.82, N'21:26:11', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2710430020001', 23, 36.54, 8.56, 221.99, 29.25, 7.93, 0.74, 0.16, 0.54, 329.09, N'21:21:54', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2710430020002', 23, 36.53, 8.56, 221.74, 29.11, 7.96, 0.74, 0.16, 0.54, 318.72, N'21:22:25', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2710430020081', 23, 36.52, 8.55, 221.73, 29.11, 7.96, 0.74, 0.16, 0.54, 472.75, N'21:23:22', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2710430020082', 23, 36.53, 8.54, 221.88, 29.11, 7.97, 0.74, 0.16, 0.54, 513.29, N'21:24:27', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2710430020083', 23, 36.52, 8.55, 221.88, 29.11, 7.97, 0.74, 0.16, 0.53, 439.43, N'21:25:06', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2710430020084', 23, 36.53, 8.55, 221.74, 29.11, 7.96, 0.74, 0.16, 0.54, 418.82, N'21:26:11', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
GO
print 'Processed 100 total records'
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2710430020089', 23, 36.52, 8.54, 221.45, 29.11, 7.95, 0.74, 0.16, 0.53, 525.63, N'21:27:27', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2710430020085', 23, 36.53, 8.55, 221.74, 29.11, 7.96, 0.74, 0.16, 0.54, 374.58, N'21:28:19', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2710430020086', 23, 36.53, 8.55, 221.74, 29.11, 7.96, 74.19, 0.16, 0.54, 371.19, N'21:29:03', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2710430020087', 23, 36.53, 8.55, 221.73, 29.11, 7.96, 74.2, 15.87, 0.53, 407.15, N'21:29:35', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2710430020088', 23, 36.52, 8.55, 221.88, 29.11, 7.97, 74.28, 15.88, 0.54, 457.1, N'21:30:06', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2710430020018', 23, 36.52, 8.55, 221.59, 29.11, 7.96, 74.19, 15.86, 0.53, 480.73, N'21:30:38', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2710430020090', 23, 36.53, 8.55, 221.85, 29.16, 7.95, 74.26, 15.88, 0.54, 442.67, N'21:31:09', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2710430020011', 23, 36.54, 8.56, 221.99, 29.25, 7.93, 0.74, 0.16, 0.54, 329.09, N'21:21:54', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2710430020012', 23, 36.53, 8.56, 221.74, 29.11, 7.96, 0.74, 0.16, 0.54, 318.72, N'21:22:25', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2710430020028', 23, 36.52, 8.55, 221.73, 29.11, 7.96, 0.74, 0.16, 0.54, 472.75, N'21:23:22', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2710430020038', 23, 36.53, 8.54, 221.88, 29.11, 7.97, 0.74, 0.16, 0.54, 513.29, N'21:24:27', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2710430020048', 23, 36.52, 8.55, 221.88, 29.11, 7.97, 0.74, 0.16, 0.53, 439.43, N'21:25:06', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
INSERT [dbo].[NewTempTable] ([FNumber], [Temp], [VOC], [ISC], [Pmax], [Vm], [Im], [FF], [Eff], [Serial], [Shunt], [Time], [Date], [Operator], [SchemeInterID], [CheckDate], [FNO]) VALUES (N'2710430020558', 23, 36.53, 8.55, 221.74, 29.11, 7.96, 0.74, 0.16, 0.54, 418.82, N'21:26:11', N'2010-12-01', N'yongxing.tian', 3, CAST(0x00009EEF0045EA0C AS DateTime), 1)
/****** Object:  Table [dbo].[ModelType]    Script Date: 05/29/2011 21:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModelType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ModelType](
	[InterID] [int] NOT NULL,
	[ModelTypeID] [nvarchar](50) NOT NULL,
	[ItemModelInterID] [int] NOT NULL,
	[ItemPmaxInterID] [int] NOT NULL,
	[NOValue] [nvarchar](50) NULL,
	[OCVValue] [nvarchar](50) NULL,
	[SCCValue] [nvarchar](50) NULL,
	[VPPValue] [nvarchar](50) NULL,
	[CPPValue] [nvarchar](50) NULL,
	[RMPValue] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[CreateUserID] [int] NULL,
	[ModifyUserID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[InterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
INSERT [dbo].[ModelType] ([InterID], [ModelTypeID], [ItemModelInterID], [ItemPmaxInterID], [NOValue], [OCVValue], [SCCValue], [VPPValue], [CPPValue], [RMPValue], [CreateDate], [ModifyDate], [CreateUserID], [ModifyUserID]) VALUES (1, N'CS5A-230', 1, 11, N'', N'45.3', N'11.2', N'15.2', N'10', N'230', CAST(0x00009EEF004079DC AS DateTime), NULL, 0, NULL)
INSERT [dbo].[ModelType] ([InterID], [ModelTypeID], [ItemModelInterID], [ItemPmaxInterID], [NOValue], [OCVValue], [SCCValue], [VPPValue], [CPPValue], [RMPValue], [CreateDate], [ModifyDate], [CreateUserID], [ModifyUserID]) VALUES (2, N'CS6P-230', 5, 11, N'', N'39.2', N'3.3', N'3', N'12', N'230', CAST(0x00009EEF00409E30 AS DateTime), NULL, 0, NULL)
INSERT [dbo].[ModelType] ([InterID], [ModelTypeID], [ItemModelInterID], [ItemPmaxInterID], [NOValue], [OCVValue], [SCCValue], [VPPValue], [CPPValue], [RMPValue], [CreateDate], [ModifyDate], [CreateUserID], [ModifyUserID]) VALUES (3, N'CS5C-180', 3, 9, N'Test01', N'15.9', N'12.3', N'16.3', N'12', N'180', CAST(0x00009EEF0040CBE4 AS DateTime), NULL, 0, NULL)
INSERT [dbo].[ModelType] ([InterID], [ModelTypeID], [ItemModelInterID], [ItemPmaxInterID], [NOValue], [OCVValue], [SCCValue], [VPPValue], [CPPValue], [RMPValue], [CreateDate], [ModifyDate], [CreateUserID], [ModifyUserID]) VALUES (4, N'CS5A-180', 1, 9, N'Test', N'32.1', N'10', N'10', N'10', N'180', CAST(0x00009EF2015D3CB0 AS DateTime), NULL, 0, NULL)
/****** Object:  Table [dbo].[Log]    Script Date: 05/29/2011 21:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Log]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Log](
	[InterID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[RunningDate] [datetime] NULL,
	[RunningNote] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[InterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ItemClass]    Script Date: 05/29/2011 21:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ItemClass]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ItemClass](
	[InterID] [int] NOT NULL,
	[ItemClassID] [nvarchar](50) NOT NULL,
	[ItemClassNM] [nvarchar](50) NOT NULL,
	[CreateDate] [datetime] NULL,
	[CreateUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[InterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
INSERT [dbo].[ItemClass] ([InterID], [ItemClassID], [ItemClassNM], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (1, N'1', N'Product Type', CAST(0x00009C0100A8186C AS DateTime), 0, NULL, NULL)
INSERT [dbo].[ItemClass] ([InterID], [ItemClassID], [ItemClassNM], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (2, N'2', N'Nominal power', CAST(0x00009C0100A82550 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[ItemClass] ([InterID], [ItemClassID], [ItemClassNM], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (3, N'3', N'Chip Type', CAST(0x00009C0100A82FDC AS DateTime), 0, NULL, NULL)
INSERT [dbo].[ItemClass] ([InterID], [ItemClassID], [ItemClassNM], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (4, N'4', N'label', CAST(0x00009C0100A87758 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[ItemClass] ([InterID], [ItemClassID], [ItemClassNM], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (5, N'5', N'brand', CAST(0x00009C0100A88FF4 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[ItemClass] ([InterID], [ItemClassID], [ItemClassNM], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (6, N'6', N'Color', CAST(0x00009C0100A89F30 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[ItemClass] ([InterID], [ItemClassID], [ItemClassNM], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (7, N'7', N'Size(MM)', CAST(0x00009C0100A8AAE8 AS DateTime), 0, NULL, NULL)
/****** Object:  Table [dbo].[Item]    Script Date: 05/29/2011 21:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Item]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Item](
	[InterID] [int] NOT NULL,
	[ItemClassInterID] [int] NOT NULL,
	[ItemID] [int] NOT NULL,
	[ItemValue] [nvarchar](50) NOT NULL,
	[CreateDate] [datetime] NULL,
	[CreateUser] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUser] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[InterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (1, 1, 1, N'CS5A', CAST(0x00009EEF00363300 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (2, 1, 2, N'CS5B', CAST(0x00009EEF00363EB8 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (3, 1, 3, N'CS5C', CAST(0x00009EEF00364818 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (4, 1, 4, N'CS6A', CAST(0x00009EEF00364F20 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (5, 1, 5, N'CS6P', CAST(0x00009EEF00365754 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (6, 2, 1, N'10', CAST(0x00009EEF00366A14 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (7, 2, 2, N'20', CAST(0x00009EEF0036711C AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (8, 2, 3, N'50', CAST(0x00009EEF00367A7C AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (9, 2, 4, N'180', CAST(0x00009EEF00368184 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (10, 2, 5, N'220', CAST(0x00009EEF0036888C AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (11, 2, 6, N'230', CAST(0x00009EEF00368E68 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (12, 3, 1, N'P', CAST(0x00009EEF0036A4AC AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (13, 3, 2, N'M', CAST(0x00009EEF0036ABB4 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (14, 3, 3, N'PE', CAST(0x00009EEF0036B2BC AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (15, 4, 1, N'Schuco Actual label', CAST(0x00009EEF0036DA94 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (16, 4, 2, N'CS label in box', CAST(0x00009EEF0036EFAC AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (17, 5, 1, N'Schuco brand', CAST(0x00009EEF00370BCC AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (18, 5, 2, N'CS brand', CAST(0x00009EEF00371C34 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (19, 6, 1, N'Blue', CAST(0x00009EEF00372EF4 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (20, 6, 2, N'Dark', CAST(0x00009EEF00373728 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (21, 7, 1, N'125*125', CAST(0x00009EEF0037440C AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (22, 7, 2, N'156*156', CAST(0x00009EEF00374C40 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (23, 1, 6, N'CS6C', CAST(0x00009EF2015CBBC8 AS DateTime), 0, NULL, NULL)
INSERT [dbo].[Item] ([InterID], [ItemClassInterID], [ItemID], [ItemValue], [CreateDate], [CreateUser], [ModifyDate], [ModifyUser]) VALUES (24, 3, 4, N'PS', CAST(0x00009EF2015E10CC AS DateTime), 0, NULL, NULL)
/****** Object:  Table [dbo].[FunctionItem]    Script Date: 05/29/2011 21:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FunctionItem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FunctionItem](
	[InterID] [int] NOT NULL,
	[FuncGroupInterID] [int] NOT NULL,
	[FuncItemID] [nvarchar](50) NOT NULL,
	[FuncItemNM] [nvarchar](50) NOT NULL,
	[FuncForm] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[InterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
INSERT [dbo].[FunctionItem] ([InterID], [FuncGroupInterID], [FuncItemID], [FuncItemNM], [FuncForm]) VALUES (0, 1, N'1.1', N'User Management maintain', N'frmUserManage')
INSERT [dbo].[FunctionItem] ([InterID], [FuncGroupInterID], [FuncItemID], [FuncItemNM], [FuncForm]) VALUES (1, 2, N'2.1', N'Based data management maintain', N'frmItemManage')
INSERT [dbo].[FunctionItem] ([InterID], [FuncGroupInterID], [FuncItemID], [FuncItemNM], [FuncForm]) VALUES (2, 3, N'3.1', N'Nominal Power of sub-file management', N'frmPmaxManage')
INSERT [dbo].[FunctionItem] ([InterID], [FuncGroupInterID], [FuncItemID], [FuncItemNM], [FuncForm]) VALUES (3, 3, N'3.2', N'Nominal power parameters', N'frmModelManage')
INSERT [dbo].[FunctionItem] ([InterID], [FuncGroupInterID], [FuncItemID], [FuncItemNM], [FuncForm]) VALUES (4, 4, N'4.1', N'Small digital management', N'frmPointManage')
INSERT [dbo].[FunctionItem] ([InterID], [FuncGroupInterID], [FuncItemID], [FuncItemNM], [FuncForm]) VALUES (5, 5, N'5.1', N'Scheme configuration', N'frmSchemeManage')
INSERT [dbo].[FunctionItem] ([InterID], [FuncGroupInterID], [FuncItemID], [FuncItemNM], [FuncForm]) VALUES (6, 6, N'6.1', N'Scheme run', N'frmSchemeRunning')
INSERT [dbo].[FunctionItem] ([InterID], [FuncGroupInterID], [FuncItemID], [FuncItemNM], [FuncForm]) VALUES (7, 7, N'7.1', N'Label printing system', N'frmLablePrint')
INSERT [dbo].[FunctionItem] ([InterID], [FuncGroupInterID], [FuncItemID], [FuncItemNM], [FuncForm]) VALUES (8, 7, N'7.2', N'Barcode printing system', N'frmLogoPrint')
INSERT [dbo].[FunctionItem] ([InterID], [FuncGroupInterID], [FuncItemID], [FuncItemNM], [FuncForm]) VALUES (9, 8, N'8.1', N'Abnormal data processing', N'frmErrManage')
INSERT [dbo].[FunctionItem] ([InterID], [FuncGroupInterID], [FuncItemID], [FuncItemNM], [FuncForm]) VALUES (11, 8, N'8.2', N'Barcode data derived', N'frmExportToExcel')
/****** Object:  Table [dbo].[FunctionGroup]    Script Date: 05/29/2011 21:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FunctionGroup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FunctionGroup](
	[InterID] [int] NOT NULL,
	[FuncGroupID] [nvarchar](50) NOT NULL,
	[FuncGroupNM] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[InterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
INSERT [dbo].[FunctionGroup] ([InterID], [FuncGroupID], [FuncGroupNM]) VALUES (1, N'1', N'User Management')
INSERT [dbo].[FunctionGroup] ([InterID], [FuncGroupID], [FuncGroupNM]) VALUES (2, N'2', N'Based data management')
INSERT [dbo].[FunctionGroup] ([InterID], [FuncGroupID], [FuncGroupNM]) VALUES (3, N'3', N'Nominal Power Management')
INSERT [dbo].[FunctionGroup] ([InterID], [FuncGroupID], [FuncGroupNM]) VALUES (4, N'4', N'Decimal places Management')
INSERT [dbo].[FunctionGroup] ([InterID], [FuncGroupID], [FuncGroupNM]) VALUES (5, N'5', N'Program configuration')
INSERT [dbo].[FunctionGroup] ([InterID], [FuncGroupID], [FuncGroupNM]) VALUES (6, N'6', N'Program run')
INSERT [dbo].[FunctionGroup] ([InterID], [FuncGroupID], [FuncGroupNM]) VALUES (7, N'7', N'Printing system')
INSERT [dbo].[FunctionGroup] ([InterID], [FuncGroupID], [FuncGroupNM]) VALUES (8, N'8', N'Data Processing')
/****** Object:  Table [dbo].[ElecParaTest]    Script Date: 05/29/2011 21:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ElecParaTest]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ElecParaTest](
	[EPTID] [bigint] IDENTITY(1,1) NOT NULL,
	[ProcessID] [bigint] NULL,
	[SN] [nvarchar](20) NOT NULL,
	[EPTDate] [datetime] NOT NULL,
	[Temperature] [float] NOT NULL,
	[Voc] [float] NOT NULL,
	[Isc] [float] NOT NULL,
	[Pmax] [float] NOT NULL,
	[Vm] [float] NOT NULL,
	[Im] [float] NOT NULL,
	[FF] [float] NOT NULL,
	[Eta] [float] NOT NULL,
	[Process] [nvarchar](2) NULL
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[ElecParaTest] ON
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (1, NULL, N'0710430020001', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.54, 8.56, 231.99, 29.25, 7.93, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (2, NULL, N'0710430020002', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.56, 231.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (3, NULL, N'0710430020081', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.52, 8.55, 231.73, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (4, NULL, N'0710430020082', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.54, 231.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (5, NULL, N'0710430020083', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.52, 8.55, 231.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (6, NULL, N'0710430020084', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.55, 231.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (7, NULL, N'0710430020089', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.52, 8.54, 231.45, 29.11, 7.95, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (8, NULL, N'0710430020085', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.55, 231.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (9, NULL, N'0710430020086', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.55, 231.74, 29.11, 7.96, 74.19, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (10, NULL, N'0710430020087', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.55, 231.73, 29.11, 7.96, 74.2, 15.87, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (11, NULL, N'0710430020088', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.52, 8.55, 231.88, 29.11, 7.97, 74.28, 15.88, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (12, NULL, N'0710430020018', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.52, 8.55, 231.59, 29.11, 7.96, 74.19, 15.86, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (13, NULL, N'0710430020090', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.55, 231.85, 29.16, 7.95, 74.26, 15.88, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (14, NULL, N'0710430020011', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.54, 8.56, 231.99, 29.25, 7.93, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (15, NULL, N'0710430020012', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.56, 231.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (16, NULL, N'0710430020028', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.52, 8.55, 231.73, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (17, NULL, N'0710430020038', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.54, 231.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (18, NULL, N'0710430020048', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.52, 8.55, 231.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (19, NULL, N'0710430020558', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.55, 231.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (20, NULL, N'0710430021001', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.54, 8.56, 231.99, 29.25, 7.93, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (21, NULL, N'0710430021002', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.56, 231.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (22, NULL, N'0710430021081', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.52, 8.55, 231.73, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (23, NULL, N'0710430021082', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.54, 231.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (24, NULL, N'0710430021083', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.52, 8.55, 231.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (25, NULL, N'0710430021084', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.55, 231.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (26, NULL, N'0710430021089', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.52, 8.54, 231.45, 29.11, 7.95, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (27, NULL, N'0710430021085', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.55, 231.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (28, NULL, N'0710430021086', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.55, 231.74, 29.11, 7.96, 74.19, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (29, NULL, N'0710430021087', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.55, 231.73, 29.11, 7.96, 74.2, 15.87, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (30, NULL, N'0710430021088', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.52, 8.55, 231.88, 29.11, 7.97, 74.28, 15.88, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (31, NULL, N'0710430021018', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.52, 8.55, 231.59, 29.11, 7.96, 74.19, 15.86, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (32, NULL, N'0710430021090', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.55, 231.85, 29.16, 7.95, 74.26, 15.88, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (33, NULL, N'0710430021011', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.54, 8.56, 231.99, 29.25, 7.93, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (34, NULL, N'0710430021012', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.56, 231.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (35, NULL, N'0710430021028', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.52, 8.55, 231.73, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (36, NULL, N'0710430021038', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.54, 231.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (37, NULL, N'0710430021048', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.52, 8.55, 231.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (38, NULL, N'0710430021558', CAST(0x00009EEF0043F800 AS DateTime), 23, 36.53, 8.55, 231.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (39, NULL, N'0810430021001', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.54, 8.56, 181.99, 29.25, 7.93, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (40, NULL, N'0810430021002', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.53, 8.56, 181.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (41, NULL, N'0810430021081', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.52, 8.55, 181.73, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (42, NULL, N'0810430021082', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.53, 8.54, 181.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (43, NULL, N'0810430021083', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.52, 8.55, 181.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (44, NULL, N'0810430021084', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.53, 8.55, 181.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (45, NULL, N'0810430021089', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.52, 8.54, 181.45, 29.11, 7.95, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (46, NULL, N'0810430021085', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.53, 8.55, 181.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (47, NULL, N'0810430021086', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.53, 8.55, 181.74, 29.11, 7.96, 74.19, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (48, NULL, N'0810430021087', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.53, 8.55, 181.73, 29.11, 7.96, 74.2, 15.87, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (49, NULL, N'0810430021088', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.52, 8.55, 181.88, 29.11, 7.97, 74.28, 15.88, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (50, NULL, N'0810430021018', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.52, 8.55, 181.59, 29.11, 7.96, 74.19, 15.86, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (51, NULL, N'0810430021090', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.53, 8.55, 181.85, 29.16, 7.95, 74.26, 15.88, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (52, NULL, N'0810430021011', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.54, 8.56, 181.99, 29.25, 7.93, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (53, NULL, N'0810430021012', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.53, 8.56, 181.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (54, NULL, N'0810430021028', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.52, 8.55, 181.73, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (55, NULL, N'0810430021038', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.53, 8.54, 181.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (56, NULL, N'0810430021048', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.52, 8.55, 181.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (57, NULL, N'0810430021558', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.53, 8.55, 181.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (58, NULL, N'8710430020001', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.54, 8.56, 181.99, 29.25, 7.93, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (59, NULL, N'8710430020002', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.53, 8.56, 181.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (60, NULL, N'8710430020081', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.52, 8.55, 181.73, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (61, NULL, N'8710430020082', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.53, 8.54, 181.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (62, NULL, N'8710430020083', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.52, 8.55, 181.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (63, NULL, N'8710430020084', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.53, 8.55, 181.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (64, NULL, N'8710430020089', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.52, 8.54, 181.45, 29.11, 7.95, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (65, NULL, N'8710430020085', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.53, 8.55, 181.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (66, NULL, N'8710430020086', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.53, 8.55, 181.74, 29.11, 7.96, 74.19, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (67, NULL, N'8710430020087', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.53, 8.55, 181.73, 29.11, 7.96, 74.2, 15.87, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (68, NULL, N'8710430020088', CAST(0x00009EEF00453A44 AS DateTime), 23, 36.52, 8.55, 181.88, 29.11, 7.97, 74.28, 15.88, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (69, NULL, N'8710430020018', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.52, 8.55, 231.59, 29.11, 7.96, 74.19, 15.86, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (70, NULL, N'8710430020090', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.55, 231.85, 29.16, 7.95, 74.26, 15.88, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (71, NULL, N'8710430020011', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.54, 8.56, 231.99, 29.25, 7.93, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (72, NULL, N'8710430020012', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.56, 231.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (73, NULL, N'8710430020028', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.52, 8.55, 231.73, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (74, NULL, N'8710430020038', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.54, 231.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (75, NULL, N'8710430020048', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.52, 8.55, 231.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (76, NULL, N'8710430020558', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.55, 231.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (77, NULL, N'2810430021001', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.54, 8.56, 221.99, 29.25, 7.93, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (78, NULL, N'2810430021002', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.56, 221.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (79, NULL, N'2810430021081', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.52, 8.55, 221.73, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (80, NULL, N'2810430021082', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.54, 221.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (81, NULL, N'2810430021083', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.52, 8.55, 221.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (82, NULL, N'2810430021084', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.55, 221.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (83, NULL, N'2810430021089', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.52, 8.54, 221.45, 29.11, 7.95, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (84, NULL, N'2810430021085', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.55, 221.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (85, NULL, N'2810430021086', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.55, 221.74, 29.11, 7.96, 74.19, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (86, NULL, N'2810430021087', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.55, 221.73, 29.11, 7.96, 74.2, 15.87, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (87, NULL, N'2810430021088', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.52, 8.55, 221.88, 29.11, 7.97, 74.28, 15.88, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (88, NULL, N'2810430021018', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.52, 8.55, 221.59, 29.11, 7.96, 74.19, 15.86, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (89, NULL, N'2810430021090', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.55, 221.85, 29.16, 7.95, 74.26, 15.88, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (90, NULL, N'2810430021011', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.54, 8.56, 221.99, 29.25, 7.93, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (91, NULL, N'2810430021012', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.56, 221.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (92, NULL, N'2810430021028', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.52, 8.55, 221.73, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (93, NULL, N'2810430021038', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.54, 221.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (94, NULL, N'2810430021048', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.52, 8.55, 221.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (95, NULL, N'2810430021558', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.55, 221.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (96, NULL, N'2710430020001', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.54, 8.56, 221.99, 29.25, 7.93, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (97, NULL, N'2710430020002', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.56, 221.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (98, NULL, N'2710430020081', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.52, 8.55, 221.73, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (99, NULL, N'2710430020082', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.54, 221.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (100, NULL, N'2710430020083', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.52, 8.55, 221.88, 29.11, 7.97, 0.74, 0.16, N'T4')
GO
print 'Processed 100 total records'
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (101, NULL, N'2710430020084', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.55, 221.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (102, NULL, N'2710430020089', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.52, 8.54, 221.45, 29.11, 7.95, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (103, NULL, N'2710430020085', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.55, 221.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (104, NULL, N'2710430020086', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.55, 221.74, 29.11, 7.96, 74.19, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (105, NULL, N'2710430020087', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.55, 221.73, 29.11, 7.96, 74.2, 15.87, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (106, NULL, N'2710430020088', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.52, 8.55, 221.88, 29.11, 7.97, 74.28, 15.88, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (107, NULL, N'2710430020018', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.52, 8.55, 221.59, 29.11, 7.96, 74.19, 15.86, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (108, NULL, N'2710430020090', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.55, 221.85, 29.16, 7.95, 74.26, 15.88, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (109, NULL, N'2710430020011', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.54, 8.56, 221.99, 29.25, 7.93, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (110, NULL, N'2710430020012', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.56, 221.74, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (111, NULL, N'2710430020028', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.52, 8.55, 221.73, 29.11, 7.96, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (112, NULL, N'2710430020038', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.54, 221.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (113, NULL, N'2710430020048', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.52, 8.55, 221.88, 29.11, 7.97, 0.74, 0.16, N'T4')
INSERT [dbo].[ElecParaTest] ([EPTID], [ProcessID], [SN], [EPTDate], [Temperature], [Voc], [Isc], [Pmax], [Vm], [Im], [FF], [Eta], [Process]) VALUES (114, NULL, N'2710430020558', CAST(0x00009EEF0045EA0C AS DateTime), 23, 36.53, 8.55, 221.74, 29.11, 7.96, 0.74, 0.16, N'T4')
SET IDENTITY_INSERT [dbo].[ElecParaTest] OFF
/****** Object:  Table [dbo].[Box]    Script Date: 05/29/2011 21:45:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Box]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Box](
	[BoxID] [nvarchar](10) NOT NULL,
	[BoxType] [nvarchar](10) NOT NULL,
	[ModelType] [nvarchar](10) NOT NULL,
	[Number] [int] NOT NULL,
	[ShelfID] [nvarchar](10) NULL,
	[Mix] [nvarchar](8) NULL
) ON [PRIMARY]
END
GO
INSERT [dbo].[Box] ([BoxID], [BoxType], [ModelType], [Number], [ShelfID], [Mix]) VALUES (N'011106700B', N'CS5A-2MP', N'', 3, N'', N'')
INSERT [dbo].[Box] ([BoxID], [BoxType], [ModelType], [Number], [ShelfID], [Mix]) VALUES (N'011106701B', N'CS5B-2MP', N'', 3, N'', N'')
/****** Object:  Default [DF__NewTempTabl__FNO__46E78A0C]    Script Date: 05/29/2011 21:45:01 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__NewTempTabl__FNO__46E78A0C]') AND parent_object_id = OBJECT_ID(N'[dbo].[NewTempTable]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__NewTempTabl__FNO__46E78A0C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[NewTempTable] ADD  DEFAULT ((0)) FOR [FNO]
END


End
GO
/****** Object:  Default [DF__Product__BoxID__5EBF139D]    Script Date: 05/29/2011 21:45:01 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Product__BoxID__5EBF139D]') AND parent_object_id = OBJECT_ID(N'[dbo].[Product]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Product__BoxID__5EBF139D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Product] ADD  DEFAULT ('') FOR [BoxID]
END


End
GO

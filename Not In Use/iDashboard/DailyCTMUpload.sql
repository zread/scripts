
	Declare @fromtime as Datetime
	Declare @totime as Datetime

	Set @fromtime = Case When Cast(GetDate() as Time) Between '07:00:00' And '19:00:00' Then
					Cast(Getdate() as Date) + Cast('07:00:00' As DateTime)
					   When Cast(GetDate() as Time) < '07:00:00' Then
					Cast(Getdate()-1 as Date) + Cast('19:00:00' As DateTime)
				  Else Cast(Getdate() as Date) + Cast('19:00:00' As DateTime) End

	Set @totime = Case When Cast(GetDate() as Time) Between '07:00:00' And '19:00:00' Then
					Cast(Getdate() as Date) + Cast('19:00:00' As DateTime)
					   When Cast(GetDate() as Time) < '07:00:00' Then
					Cast(Getdate() as Date) + Cast('07:00:00' As DateTime)
				  Else Cast(Getdate() + 1 as Date) + Cast('07:00:00' As DateTime) End
	
	--set @fromtime = '2015-04-08 07:00:00'
	--set @totime = '2015-04-08 19:00:00'	

Insert into [CAPA01DB01].[dbo].CTMStatsDaily
SELECT * FROM (

	SELECT ReturnDate, tbFinal.Line, Shift, Eff, Pmax, IdealPower, CTM, tbFinal.Qty as Qty, tbQty.Qty as TotQty, GetDate() as TimeTaken
	FROM(
		Select 
		Case When Max(Shift) = 'Night Shift' and Cast(GetDate() as Time) < '07:00:00' Then Max(Cast(Entry_On-1 as Date))
			 Else Max(Cast(Entry_On as Date)) End as ReturnDate,
		 
		Line, Max(Shift) as Shift, Avg(Eff) As Eff, Avg(Pmax) As Pmax, Max(Model) as Model, Avg(IdealPower) as IdealPower, 
		Case When Avg(IdealPower) > 0 Then Avg(Pmax)/Avg(IdealPower)*100 Else 0 End as CTM, Count(Entry_On) As Qty
		From(
			Select tb1.Entry_On, Cast(Eff as float) As EFF, Cast(Pmax as float) As PMax, tbType.Result As Model,
			Case When tbType.Result = '6X-P' and Eff > 0 Then Pmax/(156*156*Eff*72/100000)*100
				 When tbType.Result = '6P-P' and Eff > 0 Then Pmax/(156*156*Eff*60/100000)*100
				 When tbType.Result = '6X-M' and Eff > 0 Then Pmax/(156*156*Eff*72/100000)*100
				 When tbType.Result = '6P-M' and Eff > 0 Then Pmax/(156*156*Eff*60/100000)*100
			End As IdealPower,
			SUBSTRING(tb1.PU_Desc, 3, 1) As Line,

			Case When Cast(@fromtime as Time) = '07:00:00' Then 'Day Shift'
			Else 'Night Shift' End As Shift

			FROM(
			SELECT Event_ID, Event_Num, Entry_On, PU_Desc FROM [CAPA01DB01].[dbo].Events e

				Inner Join [CAPA01DB01].[dbo].Prod_Units pu on pu.PU_Id = e.PU_Id

				WHERE	pu.PU_ID > 0
					AND	Equipment_Type = 'KUKA'
					AND 
					Entry_On Between @fromtime And @totime				

			) tb1 

			Inner Join 

			(SELECT EVENT_ID, Result FROM [CAPA01DB01].[dbo].Tests t Inner Join [CAPA01DB01].[dbo].Variables v on t.var_Id = v.Var_Id 
			Where Test_Name = 'ModuleType' AND Entry_On Between @fromtime And @totime) tbType

			On tbType.Event_ID = tb1.Event_Id

			Inner Join 

			(SELECT EVENT_ID, Cast(Result as float) Pmax FROM [CAPA01DB01].[dbo].Tests t Inner Join [CAPA01DB01].[dbo].Variables v on t.var_Id = v.Var_Id 
			Where Test_Name = 'MaximumPower' AND ISNUMERIC(Result) = 1 And Entry_On Between @fromtime And @totime) tbPMax

			On tbPmax.Event_ID = tb1.Event_Id

			Inner Join

			(SELECT Event_ID, Event_Num FROM [CAPA01DB01].[dbo].EVENTS e

			Inner Join [CAPA01DB01].[dbo].Prod_Units pu on pu.PU_Id = e.PU_Id

			Where	
					Equipment_Type = 'Stringer'
				AND	pu.PU_ID > 0) tbStr
			On tb1.Event_Num = tbStr.Event_Num

			Inner Join

			(Select Cast(Result as float) Eff, Event_Id From [CAPA01DB01].[dbo].Tests t Join [CAPA01DB01].[dbo].Variables v on t.var_Id = v.Var_Id 
			Where Test_Name = 'CellEfficiency' AND ISNUMERIC(Result) = 1) tbEff

			On tbStr.Event_Id = tbEff.Event_Id
		) tbAlmost where IdealPower <> 0 And Pmax > Case When IdealPower > 280 Then 290 Else 240 End 
And Pmax < 330 Group By Line
	)tbFinal

Inner Join

(SELECT Count(Event_ID) as Qty, SUBSTRING(Max(PU_Desc), 3,1) As Line FROM
(Select Event_ID, PU_Id, Entry_On, Row_Number() Over(Partition By Event_Num Order By Event_ID DESC) rn From [CAPA01DB01].[dbo].Events
Where Entry_On Between @fromtime And @totime) e


Inner Join [CAPA01DB01].[dbo].Prod_Units pu on pu.PU_Id = e.PU_Id

WHERE	pu.PU_ID > 0
	And rn = 1
	AND Equipment_Type = 'KUKA'
	AND Entry_On Between @fromtime And @totime
	Group By SUBSTRING(PU_Desc, 3,1) 
) tbQty

On tbQty.Line = tbFinal.Line
) tb5
$files = Get-ChildItem "\\ca01a9007\JBox\LINEA" -Filter *.jpg
$failSN = ''

for ($i=0; $i -lt $files.Count; $i++) {
    $filename = $files[$i].BaseName
    $pathname = $files[$i].FullName
    $passName = $files[$i].BaseName
    $SN = $files[$i].BaseName
    
    $filename = $filename.Substring(0, $filename.IndexOf('_'))
    $filename = $filename.Substring($filename.IndexOf('-'))
    $filename = $filename.Substring($filename.IndexOf('2'))
    
    $pathname = split-path $files[$i].FullName -Parent
    $SN = $SN.Substring(0, $SN.IndexOf('_'))    
    $foldername = $pathname + '\' +  $Filename
    
    if($passName.Substring($passName.IndexOf('-p')) -eq "-pre")
    {
        $passName = $passName.Substring($passName.IndexOf('_'))
        $passName = $passName.Substring($passName.IndexOf('-'))         
        $passName = $passName.Substring(1,1)
        
        if($passName -eq 'T')
        {
            $foldername = $foldername + "\Pass"                     
        }
        else
        {
            $foldername = $foldername + "\Fail"
            $failSN = $SN
        }
    }
    elseif ($SN -eq $failSN)
    {
        $foldername = $foldername + "\Fail"
    }
    else
    {
        $foldername = $foldername + "\Pass"
    }
    
    if(Test-Path $foldername)
    {        
    }
    else
    {
        New-Item -ItemType Directory -Path $foldername
    } 
    
    if(Test-Path $foldername)
    {
        Move-Item $files[$i].FullName $foldername
    }    
}

$files = Get-ChildItem "\\ca01a9007\JBox\LINEB" -Filter *.jpg
$failSN = ''

for ($i=0; $i -lt $files.Count; $i++) {
    $filename = $files[$i].BaseName
    $pathname = $files[$i].FullName
    $passName = $files[$i].BaseName
    $SN = $files[$i].BaseName
    
    $filename = $filename.Substring(0, $filename.IndexOf('_'))
    $filename = $filename.Substring($filename.IndexOf('-'))
    $filename = $filename.Substring($filename.IndexOf('2'))
    
    $pathname = split-path $files[$i].FullName -Parent
    $SN = $SN.Substring(0, $SN.IndexOf('_'))    
    $foldername = $pathname + '\' +  $Filename
    
    if($passName.Substring($passName.IndexOf('-p')) -eq "-pre")
    {
        $passName = $passName.Substring($passName.IndexOf('_'))
        $passName = $passName.Substring($passName.IndexOf('-'))         
        $passName = $passName.Substring(1,1)
        
        if($passName -eq 'T')
        {
            $foldername = $foldername + "\Pass"                     
        }
        else
        {
            $foldername = $foldername + "\Fail"
            $failSN = $SN
        }
    }
    elseif ($SN -eq $failSN)
    {
        $foldername = $foldername + "\Fail"
    }
    else
    {
        $foldername = $foldername + "\Pass"
    }
  
    if(Test-Path $foldername)
    {        
    }
    else
    {
        New-Item -ItemType Directory -Path $foldername
    } 
    
    if(Test-Path $foldername)
    {
        Move-Item $files[$i].FullName $foldername
    }    
}

$files = Get-ChildItem "\\ca01a9007\JBox\LINED" -Filter *.jpg
$failSN = ''

for ($i=0; $i -lt $files.Count; $i++) {
    $filename = $files[$i].BaseName
    $pathname = $files[$i].FullName
    $passName = $files[$i].BaseName
    $SN = $files[$i].BaseName
    
    $filename = $filename.Substring(0, $filename.IndexOf('_'))
    $filename = $filename.Substring($filename.IndexOf('-'))
    $filename = $filename.Substring($filename.IndexOf('2'))
    
    $pathname = split-path $files[$i].FullName -Parent
    $SN = $SN.Substring(0, $SN.IndexOf('_'))    
    $foldername = $pathname + '\' +  $Filename
    
    if($passName.Substring($passName.IndexOf('-p')) -eq "-pre")
    {
        $passName = $passName.Substring($passName.IndexOf('_'))
        $passName = $passName.Substring($passName.IndexOf('-'))         
        $passName = $passName.Substring(1,1)
        
        if($passName -eq 'T')
        {
            $foldername = $foldername + "\Pass"                     
        }
        else
        {
            $foldername = $foldername + "\Fail"
            $failSN = $SN
        }
    }
    elseif ($SN -eq $failSN)
    {
        $foldername = $foldername + "\Fail"
    }
    else
    {
        $foldername = $foldername + "\Pass"
    }
  
    if(Test-Path $foldername)
    {        
    }
    else
    {
        New-Item -ItemType Directory -Path $foldername
    } 
    
    if(Test-Path $foldername)
    {
        Move-Item $files[$i].FullName $foldername
    }    
}
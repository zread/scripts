$SQLServer = "CA01S0034\SQLEXPRESS"
$user = "reader"
$password = "Reader123"
$SQLDBName = "QuickBaseMirror"
$SqlQuery = 
"SELECT Top 3 Max(dlc.[Date_Created]), dlc.[Date], line, MAX(dlc.[Shift]) As Shift,
		(ISNULL(SUM([Stringer_Area]),0) + ISNULL(SUM([Bussing_Area]),0) + ISNULL(SUM([KUKA_Area]),0)) * 12 AS Labour,
		(ISNULL(SUM([Stringer_Area_Overtime]),0) + ISNULL(SUM([Bussing_Area_Overtime]),0) + ISNULL(SUM([KUKA_Area_Overtime]),0)) * 12 AS Overtime,
		CASE When Line = 'A' Then Max(dr.Line_A___Qty__From_Day_Shift_)
			 When Line = 'B' Then Max(dr.Line_B___Qty__From_Day_Shift_)
			 Else Max(dr.Line_D___Qty__From_Day_Shift_)
		End As Rework_Qty
FROM [QuickbaseMirror].[dbo].[NPC_Activities__Direct_Labour_Counting_bjzrp9p9b] dlc 
Left Join [QuickbaseMirror].[dbo].[NPC_Activities__Rework_Daily_Report_bjzrqkv83] dr
on dlc.Date = dr.Date and SUBSTRING(dlc.Shift, 1, 1) = SUBSTRING(dr.Shift, 1, 1)
WHERE dlc.[Date_Created] Between DATEADD(HH, -17.5, GETDATE()) AND DATEADD(HH, -0, GETDATE())
AND ((CAST(GETDATE() As time) Between '07:00:00' AND '19:00:00' AND SUBSTRING(dlc.[Shift], 1, 1) = 'N') OR (CAST(GETDATE() As time) > '19:00:00' OR CAST(GETDATE() As time) < '07:00:00'))
Group By dlc.Date, SUBSTRING(dlc.shift, 1,2), line
Order by dlc.Date Desc"

$SqlConnection = New-Object System.Data.SqlClient.SqlConnection
$SqlConnection.ConnectionString = "Server=$SQLServer;Database=$SQLDBName;User ID=$user;Password=$password;"
 
$SqlCmd = New-Object System.Data.SqlClient.SqlCommand
$SqlCmd.CommandText = $SqlQuery
$SqlCmd.Connection = $SqlConnection
 
$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
$SqlAdapter.SelectCommand = $SqlCmd
 
$DataSet = New-Object System.Data.DataSet
$SqlAdapter.Fill($DataSet)
 
$SqlConnection.Close()

$SQLServer = "CA01A0047"
$user = "iDashboards"
$password = "solardash"
$SQLDBName = "CAPA01DB01"
$SqlConnection = New-Object System.Data.SqlClient.SqlConnection
$connectionString = "Server=$dataSource;uid=$user; pwd=$pwd;Database=$database;Integrated Security=False;"
$SqlCmd = New-Object System.Data.SqlClient.SqlCommand

for($i=0;$i -le $DataSet.Tables[0].Rows.Count -1;$i++)
{
    $SqlQuery = "Insert into LabourHoursPerLine (LabourDay, line, Shifts, LabourHours, OTHours, Rework)
    Values ('"+ $DataSet.Tables[0].Rows[$i][0] +"', '" +  $DataSet.Tables[0].Rows[$i][1] +"', '" +  $DataSet.Tables[0].Rows[$i][2] + "', '" +  
    $DataSet.Tables[0].Rows[$i][3] + "', '" +  $DataSet.Tables[0].Rows[$i][4] + "', '" +  $DataSet.Tables[0].Rows[$i][5] +"')";
               	
    write-output $sqlQuery
    $SqlConnection.ConnectionString = "Server=$SQLServer;Database=$SQLDBName;User ID=$user;Password=$password;"
    $SqlConnection.Open()
    $SqlCmd = $SqlConnection.CreateCommand()
    $SqlAdapter.InsertCommand = $SqlCmd 
    $SqlCmd.CommandText = $SqlQuery
    $SqlCmd.Connection = $SqlConnection		 
    		
    $SqlCmd.ExecuteNonQuery()		 
    $SqlConnection.Close()
}
function compareSetting
{
    param($setting, $counter, $machine, $line, $type)
        
    if(!$setting.Equals([DBNull]::Value) -And !$counter.Equals([DBNull]::Value))
    {
        write-host $setting ",   " $counter
        if([int]$setting -lt ([int]$counter + [int]$Replace))
        { 
            $messageNeededEB = 0
            $messageNeededGE = 0 
            $messageNeededG = 0 
            $messageNeededD = 0
            
            $smtpServer = "csica01ms01.csisolar.com"
            $msg = new-object Net.Mail.MailMessage
            $smtp = new-object Net.Mail.SmtpClient($smtpServer)
            $msg.From = "GuelphProduction.Err@canadiansolar.com"
            #$msg.To.Add("Zach.Read@canadiansolar.com")
            $msg.subject = "Warning: the following machines need material resupplying"    
            $strMessage = "The following machines need material resupplying.`n"            
            
            $differnce = $setting - $counter
            $strMessage = $strMessage + $machine + " needs to have its material soon, " + $differnce + " more panels can be created before the material runs out `n"
            if($type -eq "s"){
                $messageNeededG = 1
                if($Line -eq "D"){                 
                    $messageNeededD = 1
                }
                else{
                    $messageNeededGE = 1
                }
            }
            elseif($type -eq "1"){
                if($Line -eq "D"){                 
                    $messageNeededD = 1
                }
                else{
                    $messageNeededGE = 1
                }
            }
            else{
                if($Line -eq "D"){                 
                    $messageNeededD = 1
                }
                else{
                    $messageNeededEB = 1
                }
            }
        }
    }
    
    if($messageNeededEB -eq 1)
    {
        $msg.To.Add("EB.Loading.AB@canadiansolar.com")       
    }    
    if($messageNeededGE -eq 1)
    {
        $msg.To.Add("GE.Loading.AB@canadiansolar.com")        
    }    
    if($messageNeededG -eq 1)
    {
        $msg.To.Add("Glass.Forklift@canadiansolar.com")
    }    
    if($messageNeededD -eq 1)
    {
        $msg.To.Add("GEB.Loading.D@canadiansolar.com")
    }
    
    if($messageNeededEB -eq 1 -Or $messageNeededGE -eq 1 -Or $messageNeededG -eq 1 -Or $messageNeededD -eq 1){
        $msg.body = "Hello, this is an automated message. `n `n" + $strMessage
        $smtp.Send($msg)
    }
}

$Replace = 10
$SQLServer = "CA01A0047"
$user = "reader"
$password = "12345"
$SQLDBName = "CAPA01DB01"
$SqlQuery = 
"Select top 39 Result, SUBSTRING(Var_Desc, 1,CHARINDEX('_',Var_Desc)-1) As Machine, SUBSTRING(Var_Desc, 2, 1) As Line, SUBSTRING(Var_Desc, 7, 1) As Type from Tests t join variables v on t.Var_Id = v.Var_Id
Where Test_Name in ('EVA1', 'EVA2', 'BackSheet', 'Glass') Order by Result_On Desc, t.Var_Id"

$SqlConnection = New-Object System.Data.SqlClient.SqlConnection
$SqlConnection.ConnectionString = "Server=$SQLServer;Database=$SQLDBName;User ID=$user;Password=$password;"

$SqlCmd = New-Object System.Data.SqlClient.SqlCommand
$SqlCmd.CommandText = $SqlQuery
$SqlCmd.Connection = $SqlConnection 
$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
$SqlAdapter.SelectCommand = $SqlCmd
$DataSet = New-Object System.Data.DataSet
$SqlAdapter.Fill($DataSet) 
$SqlConnection.Close()

$todayDate = (get-date).DayOfWeek

compareSetting ($DataSet.Tables[0].Rows[0][0]) ($DataSet.Tables[0].Rows[1][0]) ($DataSet.Tables[0].Rows[0][1]) ($DataSet.Tables[0].Rows[0][2]) ($DataSet.Tables[0].Rows[0][3])
compareSetting ($DataSet.Tables[0].Rows[2][0]) ($DataSet.Tables[0].Rows[3][0]) ($DataSet.Tables[0].Rows[2][1]) ($DataSet.Tables[0].Rows[2][2]) ($DataSet.Tables[0].Rows[2][3])
compareSetting ($DataSet.Tables[0].Rows[4][0]) ($DataSet.Tables[0].Rows[5][0]) ($DataSet.Tables[0].Rows[4][1]) ($DataSet.Tables[0].Rows[4][2]) ($DataSet.Tables[0].Rows[4][3])
compareSetting ($DataSet.Tables[0].Rows[6][0]) ($DataSet.Tables[0].Rows[7][0]) ($DataSet.Tables[0].Rows[6][1]) ($DataSet.Tables[0].Rows[6][2]) ($DataSet.Tables[0].Rows[6][3])
compareSetting ($DataSet.Tables[0].Rows[8][0]) ($DataSet.Tables[0].Rows[9][0]) ($DataSet.Tables[0].Rows[8][1]) ($DataSet.Tables[0].Rows[8][2]) ($DataSet.Tables[0].Rows[8][3])
compareSetting ($DataSet.Tables[0].Rows[10][0]) ($DataSet.Tables[0].Rows[11][0]) ($DataSet.Tables[0].Rows[10][1]) ($DataSet.Tables[0].Rows[10][2]) ($DataSet.Tables[0].Rows[10][3])
compareSetting ($DataSet.Tables[0].Rows[12][0]) ($DataSet.Tables[0].Rows[13][0]) ($DataSet.Tables[0].Rows[12][1]) ($DataSet.Tables[0].Rows[12][2]) ($DataSet.Tables[0].Rows[12][3])
compareSetting ($DataSet.Tables[0].Rows[14][0]) ($DataSet.Tables[0].Rows[15][0]) ($DataSet.Tables[0].Rows[14][1]) ($DataSet.Tables[0].Rows[14][2]) ($DataSet.Tables[0].Rows[14][3])

compareSetting ($DataSet.Tables[0].Rows[17][0]) ($DataSet.Tables[0].Rows[18][0]) ($DataSet.Tables[0].Rows[17][1]) ($DataSet.Tables[0].Rows[17][2]) ($DataSet.Tables[0].Rows[17][3])
compareSetting ($DataSet.Tables[0].Rows[16][0]) ($DataSet.Tables[0].Rows[23][0]) ($DataSet.Tables[0].Rows[23][1]) ($DataSet.Tables[0].Rows[23][2]) ($DataSet.Tables[0].Rows[23][3])
compareSetting ($DataSet.Tables[0].Rows[19][0]) ($DataSet.Tables[0].Rows[20][0]) ($DataSet.Tables[0].Rows[19][1]) ($DataSet.Tables[0].Rows[19][2]) ($DataSet.Tables[0].Rows[19][3])
compareSetting ($DataSet.Tables[0].Rows[21][0]) ($DataSet.Tables[0].Rows[22][0]) ($DataSet.Tables[0].Rows[21][1]) ($DataSet.Tables[0].Rows[21][2]) ($DataSet.Tables[0].Rows[21][3])
compareSetting ($DataSet.Tables[0].Rows[25][0]) ($DataSet.Tables[0].Rows[26][0]) ($DataSet.Tables[0].Rows[25][1]) ($DataSet.Tables[0].Rows[25][2]) ($DataSet.Tables[0].Rows[25][3])
compareSetting ($DataSet.Tables[0].Rows[27][0]) ($DataSet.Tables[0].Rows[28][0]) ($DataSet.Tables[0].Rows[27][1]) ($DataSet.Tables[0].Rows[27][2]) ($DataSet.Tables[0].Rows[27][3])
compareSetting ($DataSet.Tables[0].Rows[29][0]) ($DataSet.Tables[0].Rows[30][0]) ($DataSet.Tables[0].Rows[29][1]) ($DataSet.Tables[0].Rows[29][2]) ($DataSet.Tables[0].Rows[29][3])
compareSetting ($DataSet.Tables[0].Rows[31][0]) ($DataSet.Tables[0].Rows[32][0]) ($DataSet.Tables[0].Rows[31][1]) ($DataSet.Tables[0].Rows[31][2]) ($DataSet.Tables[0].Rows[31][3])
compareSetting ($DataSet.Tables[0].Rows[35][0]) ($DataSet.Tables[0].Rows[36][0]) ($DataSet.Tables[0].Rows[35][1]) ($DataSet.Tables[0].Rows[35][2]) ($DataSet.Tables[0].Rows[35][3])
compareSetting ($DataSet.Tables[0].Rows[37][0]) ($DataSet.Tables[0].Rows[38][0]) ($DataSet.Tables[0].Rows[37][1]) ($DataSet.Tables[0].Rows[37][2]) ($DataSet.Tables[0].Rows[37][3])


compareSetting 100 ($DataSet.Tables[0].Rows[24][0]) ($DataSet.Tables[0].Rows[24][1]) ($DataSet.Tables[0].Rows[24][2]) ($DataSet.Tables[0].Rows[24][3])
compareSetting 100 ($DataSet.Tables[0].Rows[33][0]) ($DataSet.Tables[0].Rows[33][1]) ($DataSet.Tables[0].Rows[33][2]) ($DataSet.Tables[0].Rows[33][3])
compareSetting 100 ($DataSet.Tables[0].Rows[34][0]) ($DataSet.Tables[0].Rows[34][1]) ($DataSet.Tables[0].Rows[34][2]) ($DataSet.Tables[0].Rows[34][3])
#Need to use
#24,33,34


if($messageNeeded -eq 1)
{
    #write-host $strMessage
    #$msg.body = "Hello, this is an automated message. `n `n" + $strMessage
    #$smtp.Send($msg)
}

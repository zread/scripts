$SQLServer = "CA01A0047"
$user = "reader"
$password = "12345"
$SQLDBName = "CAPA01DB01"
$SqlQuery = 
"Select Top 27 Result, SubString(Var_Desc, 0, 14) as Name from tests t join variables v on v.Var_Id = t.Var_Id
Where Test_Name = 'BadgeReader' AND Result_On > DateAdd(d, -1, Cast(Getdate() as date)) Order By Result_On Desc"

$SqlConnection = New-Object System.Data.SqlClient.SqlConnection
$SqlConnection.ConnectionString = "Server=$SQLServer;Database=$SQLDBName;User ID=$user;Password=$password;"

$SqlCmd = New-Object System.Data.SqlClient.SqlCommand
$SqlCmd.CommandText = $SqlQuery
$SqlCmd.Connection = $SqlConnection 
$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
$SqlAdapter.SelectCommand = $SqlCmd
$DataSet = New-Object System.Data.DataSet
$SqlAdapter.Fill($DataSet) 
$SqlConnection.Close()

$smtpServer = "csica01ms01.csisolar.com"
$msg = new-object Net.Mail.MailMessage
$smtp = new-object Net.Mail.SmtpClient($smtpServer)
$msg.From = "GuelphProduction.Err@canadiansolar.com"
$msg.To.Add("Zach.Read@canadiansolar.com")
$msg.To.Add("Quality_Tech_Auditors@canadiansolar.com")
$msg.subject = "Warning: the following stations are bypassed"    
$strMessage = "The following stations are bypassed.`n"   

$x = 0
Write-Output $x
Write-Output $DataSet.Tables[0].Rows.Count
While($DataSet.Tables[0].Rows.Count-1 -gt $x)
{
	if($DataSet.Tables[0].Rows[$x][0] -eq -1){
		$strMessage = $strMessage + $DataSet.Tables[0].Rows[$x][1] + "`n"   
	}
    $x = $x + 1
}

$msg.body = "Hello, this is an automated message. `n `n" + $strMessage
$smtp.Send($msg)
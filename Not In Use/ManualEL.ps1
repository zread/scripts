﻿$threshold = 4
$SQLServer = "CA01A0047"
$user = "reader"
$password = "12345"
$SQLDBName = "CAPA01DB01"
$TodayDate = Get-Date

$SqlQuery = 
"select Result, Result_On, Var_Id from tests where Test_Id in (Select Max(Test_Id) From tests where var_id in (2907, 2908) group by var_id) Order by Var_Id"

$smtpServer = "csica01ms01.csisolar.com"
$msg = new-object Net.Mail.MailMessage
$smtp = new-object Net.Mail.SmtpClient($smtpServer)
$msg.From = "GuelphProduction.Err@canadiansolar.com"
#$msg.ReplyTo = "replyto@xxxx.com"
$msg.To.Add("Zach.Read@canadiansolar.com")
$msg.To.Add("Quality_Tech_Auditors@canadiansolar.com")
$msg.To.Add("Coulson.Barlow@canadiansolar.com")
$msg.To.Add("ProductionSupervisors@canadiansolar.com")

$SqlConnection = New-Object System.Data.SqlClient.SqlConnection
$SqlConnection.ConnectionString = "Server=$SQLServer;uid=$user; pwd=$password;Database=$SQLDBName;"
 
$SqlCmd = New-Object System.Data.SqlClient.SqlCommand
$SqlCmd.CommandText = $SqlQuery
$SqlCmd.Connection = $SqlConnection
 
$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
$SqlAdapter.SelectCommand = $SqlCmd
 
$DataSet = New-Object System.Data.DataSet
$SqlAdapter.Fill($DataSet)
 
$SqlConnection.Close()

$Line = ""

$strMessage = "Alert manual EL.`n" 
$messageNeeded = 0 


if($DataSet.Tables[0].Rows[0][0] -eq 0 -And (Get-Date) - $DataSet.Tables[0].Rows[0][1] -gt 60){
    $Line = "A"
    $strMessage = "" + $strMessage + "Line A has been in manual mode since " + $DataSet.Tables[0].Rows[0][1] + ".`n"
    $messageNeeded = 1
}
if($DataSet.Tables[0].Rows[1][0] -eq 0 -And (Get-Date) - $DataSet.Tables[0].Rows[1][1] -gt 60){
    $Line = "A"
    $strMessage = "" + $strMessage + "Line B has been in manual mode since " + $DataSet.Tables[0].Rows[1][1] + ".`n"
    $messageNeeded = 1
}


write $DataSet.Tables[0].Rows[0][0]
write $DataSet.Tables[0].Rows[1][0]
write $messageNeeded

if($messageNeeded -eq 1)
{    
    $msg.body = "Hello, this is an automated message. `n `n" + $strMessage
	$msg.subject = "Alert manual EL."	
    $smtp.Send($msg)
}

Start-Sleep -s 2
Remove-Variable strMessage
Remove-Variable Line
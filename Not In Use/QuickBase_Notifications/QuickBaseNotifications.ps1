$SQLServer = "CA01S0034\sqlexpress"
$user = "iDashboards"
$password = "solarquick"
$SQLDBName = "QuickbaseMirror"
$connectionString = “Server=$dataSource;uid=$user; pwd=$password;Database=$database;Integrated Security=False;”
$SqlQuery = 
"Select Top 30 Date_Created, Stringer, Line__, 
Case When Date_Created < DateAdd(HH, - 8, GetDate()) Then 1
	 Else 0 
End As SendEmail,
Case When SUBSTRING(Line__, 1, 2) = 'A' And (SUBSTRING(Stringer, 1, 2) = 'S1' OR SUBSTRING(Stringer, 1, 2) = 'S5') Then 'Monday'
	 When SUBSTRING(Stringer, 1, 2) = 'S1' Then 'Monday'
	 When SUBSTRING(Line__, 1, 2) = 'A' And (SUBSTRING(Stringer, 1, 2) = 'S2' OR SUBSTRING(Stringer, 1, 2) = 'S6')  Then 'Tuesday'
	 When SUBSTRING(Stringer, 1, 2) = 'S2' Then 'Tuesday'
	 When SUBSTRING(Line__, 1, 2) = 'A' And SUBSTRING(Stringer, 1, 2) = 'S3' Then 'Wednesday'	 
	 When SUBSTRING(Line__, 1, 2) = 'A' and SUBSTRING(Stringer, 1, 2) = 'S4' Then 'Thursday'
End As DayWeek
From (
Select Date_Created, Stringer, Row_Number() OVER(PARTITION BY Stringer, Line__ ORDER BY Date_Created DESC) rn, Line__  from NPC_Activities__Line_Data_bii6t76k6
) tb1 where rn = 1
and Line__ != 'D'"

$smtpServer = "csica01ms01.csisolar.com"
$msg = new-object Net.Mail.MailMessage
$smtp = new-object Net.Mail.SmtpClient($smtpServer)
$msg.From = "GuelphProduction.Err@canadiansolar.com"
#$msg.ReplyTo = "replyto@xxxx.com"
$msg.To.Add("Zach.Read@canadiansolar.com")
$msg.To.Add("Eric.Castro@canadiansolar.com")
$msg.To.Add("ProductionSupervisors@canadiansolar.com")
$msg.Cc.Add("Quality_Tech_Auditors@canadiansolar.com")
$msg.Cc.Add("Devin.Cao@canadiansolar.com")
$msg.Cc.Add("Yuehua.Yu@canadiansolar.com")
$msg.Cc.Add("Danny.Mitchel@canadiansolar.com")
$msg.subject = "Warning: Pull Test On The Following Stringers Has Not Been Performed"


$SqlConnection = New-Object System.Data.SqlClient.SqlConnection
$SqlConnection.ConnectionString = "Server = $SQLServer;uid=$user; pwd=$password; Database = $SQLDBName; Integrated Security = False"
 
$SqlCmd = New-Object System.Data.SqlClient.SqlCommand
$SqlCmd.CommandText = $SqlQuery
$SqlCmd.Connection = $SqlConnection
 
$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
$SqlAdapter.SelectCommand = $SqlCmd
 
$DataSet = New-Object System.Data.DataSet
$SqlAdapter.Fill($DataSet)
 
$SqlConnection.Close()
 
clear

$todayDate = (get-date).DayOfWeek
$strMessage = "The following stringers do not have their required pull test for this shift. Please ensure that this test is performed immediately.`n" 
$messageNeeded = 0 

for($i=0;$i -le $DataSet.Tables[0].Rows.Count - 1 ;$i++)
{ 
    if($todayDate -eq $DataSet.Tables[0].Rows[$i][4])
    {
        if($DataSet.Tables[0].Rows[$i][3] -eq 1)
        {
            $strMessage = "" + $strMessage + "Line " + $DataSet.Tables[0].Rows[$i][2]  + " " + $DataSet.Tables[0].Rows[$i][1] + "`n"
            $messageNeeded = 1
        }
    }
}

clear

if($messageNeeded -eq 1)
{
    write-host $strMessage
    $msg.body = "Hello, this is an automated message. `n `n" + $strMessage
    $smtp.Send($msg)
}
Start-Sleep -s 10
Remove-Variable strMessage

Exit
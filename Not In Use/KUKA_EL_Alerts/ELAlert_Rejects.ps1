﻿$threshold = 4
$SQLServer = "CA01S0015"
$user = "reader"
$password = "12345"
$SQLDBName = "CSILAbelPrintDB"
$TodayDate = Get-Date
$SqlQuery = 
"Select Count(SN) as Qty, Line From QualityJudge Where Remark = 'Auto KUKA EL Reject' and Timestamp > DATEADD(HH,-1,GetDate()) Group by Line"

$smtpServer = "csica01ms01.csisolar.com"
$msg = new-object Net.Mail.MailMessage
$smtp = new-object Net.Mail.SmtpClient($smtpServer)
$msg.From = "GuelphProduction.Err@canadiansolar.com"
#$msg.ReplyTo = "replyto@xxxx.com"
$msg.To.Add("Zach.Read@canadiansolar.com")
$msg.To.Add("CAMO_Engineering@canadiansolar.com")
$msg.To.Add("ProductionSupervisors@canadiansolar.com")
$msg.To.Add("Quality_Dept@canadiansolar.com")
#$msg.To.Add("Sastic.Ramachandran@canadiansolar.com")



$SqlConnection = New-Object System.Data.SqlClient.SqlConnection
$SqlConnection.ConnectionString = "Server=$SQLServer;uid=$user; pwd=$password;Database=$SQLDBName;"
 
$SqlCmd = New-Object System.Data.SqlClient.SqlCommand
$SqlCmd.CommandText = $SqlQuery
$SqlCmd.Connection = $SqlConnection
 
$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
$SqlAdapter.SelectCommand = $SqlCmd
 
$DataSet = New-Object System.Data.DataSet
$SqlAdapter.Fill($DataSet)
 
$SqlConnection.Close()

$Line = ""
$rejects = ""
$strMessage = "" 
$messageNeeded = 0 

if($DataSet.Tables[0].Rows.Count -gt 0)
{
    if($DataSet.Tables[0].Rows[0][0] -gt 4)
    { 
        $strMessage = "There have been " + $DataSet.Tables[0].Rows[0][0] + " KUKA EL Rejects on Line " + $DataSet.Tables[0].Rows[0][1]
        $msg.body = "Hello, this is an automated message. `n `n" + $strMessage
	    $msg.subject = "Large Number Of KUKA EL Rejects"	
        $smtp.Send($msg)
    }
}

Start-Sleep -s 2
Remove-Variable strMessage
Remove-Variable Line
Remove-Variable rejects

$Line = ""
$rejects = ""
$strMessage = "" 

if($DataSet.Tables[0].Rows.Count -gt 1)
{
    if($DataSet.Tables[0].Rows[1][0] -gt 4)
    { 
        $strMessage = "There have been " + $DataSet.Tables[0].Rows[0][0] + " KUKA EL Rejects on Line " + $DataSet.Tables[0].Rows[0][1]
        $msg.body = "Hello, this is an automated message. `n `n" + $strMessage
	    $msg.subject = "Large Number Of KUKA EL Rejects"	
        $smtp.Send($msg)
    }
}

Start-Sleep -s 2
Remove-Variable strMessage
Remove-Variable Line
Remove-Variable rejects
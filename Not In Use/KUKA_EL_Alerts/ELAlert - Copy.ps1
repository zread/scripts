﻿$threshold = 4
$SQLServer = "CA01A0047"
$user = "reader"
$password = "12345"
$SQLDBName = "CAPA01DB01"
$TodayDate = Get-Date
$SqlQuery = 
"SELECT Case When ISNull(Max(A),0) = 0 Then 0 Else Max(A) End As A,
Case When ISNull(Max(B),0) = 0 Then 0 Else Max(B) End As B,
Case When ISNull(Max(D),0) = 0 Then 0 Else Max(D) End As D
FROM(
SELECT CASE WHEN Substring(Input_Tag,7,1) = 'A' Then Result End As A, CASE WHEN Substring(Input_Tag,7,1) = 'B' Then Result End As B,
CASE WHEN Substring(Input_Tag,7,1) = 'D' Then Result End As D From Tests t Join
Variables v on t.var_Id = v.var_id
WHERE Test_Name = 'RejectEmail' AND Result_On > DATEADD(MI, -50, GetDate()) 
) tb1"

$smtpServer = "csica01ms01.csisolar.com"
$msg = new-object Net.Mail.MailMessage
$smtp = new-object Net.Mail.SmtpClient($smtpServer)
$msg.From = "GuelphProduction.Err@canadiansolar.com"
#$msg.ReplyTo = "replyto@xxxx.com"
$msg.To.Add("Zach.Read@canadiansolar.com")
#$msg.To.Add("ProductionSupervisors@canadiansolar.com")
$msg.To.Add("Coulson.Barlow@canadiansolar.com")



$SqlConnection = New-Object System.Data.SqlClient.SqlConnection
$SqlConnection.ConnectionString = "Server=$SQLServer;uid=$user; pwd=$password;Database=$SQLDBName;"
 
$SqlCmd = New-Object System.Data.SqlClient.SqlCommand
$SqlCmd.CommandText = $SqlQuery
$SqlCmd.Connection = $SqlConnection
 
$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
$SqlAdapter.SelectCommand = $SqlCmd
 
$DataSet = New-Object System.Data.DataSet
$SqlAdapter.Fill($DataSet)
 
$SqlConnection.Close()

$Line = ""
$rejects = ""

$strMessage = "The following line(s) are experiencing a large number of EL KUKA rejects.`n" 
$messageNeeded = 0 


if($DataSet.Tables[0].Rows[0][0] -gt 2){
    $Line = "A"
    $rejects = $DataSet.Tables[0].Rows[0][0]
    $strMessage = "" + $strMessage + "Line A with " + $rejects + " rejects`n"
    $messageNeeded = 1
}
if($DataSet.Tables[0].Rows[0][1] -gt 2){
    $Line = "B"
    $rejects = $DataSet.Tables[0].Rows[0][1]
    $strMessage = "" + $strMessage + "Line B with " + $rejects + " rejects`n"
    $messageNeeded = 1
}
#if($DataSet.Tables[0].Rows[0][2] -gt 2){
#    $Line = "D"
#    $rejects = $DataSet.Tables[0].Rows[0][2]
#    $strMessage = "" + $strMessage + "Line D with " + $rejects + " rejects`n"
#    $messageNeeded = 1
#}


if($messageNeeded -eq 1)
{    
    $msg.body = "Hello, this is an automated message. `n `n" + $strMessage
	$msg.subject = "The following line(s) are experiencing a large number of EL KUKA rejects."	
    $smtp.Send($msg)
}

Start-Sleep -s 2
Remove-Variable strMessage
Remove-Variable Line
Remove-Variable rejects

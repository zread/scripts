# Script Project README

## Running on CA01S0040
### Yield_Updates.ps1
- This script runs the Yield_Datas stored procedure which updates the Yield_Data table

### Sorting_EL2.ps1
- This script sorts all of EL2 images into subfolders

### EL3A-.ps1
- This script marks all panels in the rejected folder as A-

### EL3ImageChecker.ps1
- This script marks all panels as A- if it can't find its image

## EL3 Computers
### Move_To_Z_drive_EOL_EL_Tester.ps1
- This program moves all images from where EL3 saves them to a backup drive.

### Sorting_EL3.ps1
- This script sorts all EL3 images on the local and network drives.

## EL2 Computer
### MoveImages.bat
- This script runs on both EL2 computers and transfers images to CA01A9004.

## Running on CA01s0033
### Barcode Scanner Program
- This program monitors the barcode scanner after the auto printer to detect duplicates or incorrect SN's. This program is not longer overly used.

## Running on CA01A0046
None

## Running on CA01A0047
None

## Running on CA01s0015
None

## Running on CA01s0034
None

## CA01A0051
None

## CA01S0021
None


﻿$Dest_Dir_good = "\\ca01a9004\EL_Tester\EOL_EL_Tester\Accepted"
$Dest_Dir_bad = "\\ca01a9004\EL_Tester\EOL_EL_Tester\RejectedA-"
$Dest_Dir_bad_2 = "\\ca01a9004\EL_Tester\EOL_EL_Tester\Rejected"
$Stored_Dest = "\\ca01a9004\EL_Tester\EOL_EL_Tester\Stored"
$Stored_Dest_Local = "Z:\EL_Tester\EOL_EL_Tester\Stored"

$Dest_local_Dir_good = "Z:\EL_Tester\EOL_EL_Tester\Accepted"
$Dest_local_Dir_bad = "Z:\EL_Tester\EOL_EL_Tester\Rejected"
$Dest_local_Dir_good2 = "Z:\EL_Tester\EOL_EL_Testers\Accepted"
$Dest_local_Dir_bad2 =  "Z:\EL_Tester\EOL_EL_Testers\Rejected"

#$Dest_local_Dir_good = "C:\Users\Zach.Read\Desktop\Pic Test\Local"
#$Dest_Dir_good = "C:\Users\Zach.Read\Desktop\Pic Test\Good"
#$Stored_Dest = "C:\Users\Zach.Read\Desktop\Pic Test\Stored"

Function Move_SN
{
    Param ([string]$Src, [string]$Dest, [string]$Copy)
    Get-ChildItem -Path $Src -Filter *.jpg -Recurse | ForEach-Object {
        $num = 0
        if($_.name.length -gt 12){
            $nextName = Join-Path -Path $Dest -ChildPath $_.name

            while(Test-Path -Path $nextName)
            {
               $nextName = Join-Path $Dest ($_.BaseName + "_$num" + $_.Extension)    
               $num+=1   
            }
            write-host $nextName
            if($Copy -eq 1){
                $_ | COPY-Item -Destination $nextName
            }
            else {
                $_ | Move-Item -Destination $nextName
            }
        }
    }
}

Function Store_SN
{
    Param ([string]$Src, [string]$Dest, [string]$Grade)
    Get-ChildItem $Src -recurse | 
    Foreach-Object {
        $counter = 0
        #write $_.LastWriteTime
        if($_.name.length -gt 12){
            if($_.LastWriteTime -lt (get-date).AddHours(-12)){
                
                $pathFull = $Dest + "\Images" + "\" + $_.Name.Substring(1,2) + "\" + $_.Name.Substring(5,2) + "\" + $_.Name.Substring(7,3) + "\" + $Grade            
                
                if(!(Test-Path $pathFull))
                {
                    New-Item -Path $pathFull -ItemType Directory
                }
            
                while([System.IO.File]::Exists("$($pathFull)\$($_.basename)_$($counter)$($_.extension)"))
        		{			
        			$counter = $counter + 1
        		}
                
        		#write "$($pathFull)\3$($_.basename)_$($counter)$($_.extension)"
                #Copy-Item $_.FullName -Destination "$($pathFull)\3$($_.Name)" -PassThru 
        		Copy-Item $_.FullName -Destination "$($pathFull)\$($_.basename)_$($counter)$($_.extension)" -PassThru           
        		
        		#write $_.FullName
        	
                if($?){
        	        Remove-Item $_.FullName
                    #write "Delete"
                }
            }
       }    
    }
}

Move_SN $Dest_local_Dir_good $Dest_Dir_good "1"
Move_SN $Dest_local_Dir_good $Dest_local_Dir_good2 "0"
Move_SN $Dest_local_Dir_bad $Dest_Dir_bad "1"
Move_SN $Dest_local_Dir_bad $Dest_local_Dir_bad2 "0"

Store_SN $Dest_Dir_good $Stored_Dest "Accepted"
Store_SN $Dest_local_Dir_good2 $Stored_Dest_Local "Accepted"
Store_SN $Dest_Dir_bad_2 $Stored_Dest "Rejected"
Store_SN $Dest_local_Dir_bad2 $Stored_Dest_Local "Rejected"

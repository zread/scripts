' This script is used to find the quantity of panels per MO

Const ForReading = 1
Const ForWriting = 2	
Const ForAppending = 8

Dim tempPanel
Dim counter

counter = 0

Set fso = CreateObject("Scripting.FileSystemObject")
findYear = split(Date(), "/")

' Finds the time and date the next shift ends at
curTime = Time()
' Checks to find what shift just ended
If curTime >= TimeSerial(7,0,0) Then
	If curTime >= TimeSerial(19,0,0) Then
		startSearchTime = TimeSerial(7,0,1)
		endSearchTime = TimeSerial(19,0,0)		
		endDate = Date()
		startDate = Date()
		shift = "Day Shift"
	Else
		startSearchTime = TimeSerial(19,0,1)
		endSearchTime = TimeSerial(7,0,0)
		endDate = Date()
		startDate = Date() - 1
		shift = "Night Shift"
	End If
Else
	startSearchTime = TimeSerial(7,0,1)
	endSearchTime = TimeSerial(19,0,0)
	endDate = Date() - 1
	startDate = Date() - 1
	shift = "Day Shift"
End If	

startTime = FormatDateTime(startDate + startSearchTime, 0)
endTime = FormatDateTime(endDate + endSearchTime, 0)

For Each File In fso.GetFolder("C:\BarcodeFTP\Data_Files\" & findYear(2)).Files	
	Set theFile = fso.OpenTextFile(File.Path, ForReading)

	If File.DateLastModified > (endDate - 1 + startSearchTime - 1) Then
		Do Until theFile.AtEndOfStream	
			tempPanel = theFile.ReadLine			
			testArr = split(tempPanel,", ")
			tempPanel = testArr(1)			
			testTime = FormatDateTime(tempPanel, 0)
			If DateDiff("s", testTime, startTime) < 0 And DateDiff("s", testTime, endTime) > 0 Then
				counter = counter + 1
			End If			
		Loop
	End If
Next

Set objFile = fso.OpenTextFile("C:\BarcodeFTP\Glass_Per_Shift.csv", ForReading)
strContents = objFile.ReadAll
objFile.Close

Set objFile = fso.OpenTextFile("C:\BarcodeFTP\Glass_Per_Shift.csv", ForWriting)
objFile.Write startDate & "," & shift & "," & counter & vbCRLF & strContents
objFile.Close

Set objFile = fso.OpenTextFile("C:\BarcodeFTP\Glass_Per_Shift.txt", ForReading)
strContents = objFile.ReadLine
lastRecord = objFile.Readline
objFile.Close

Set objFile = fso.OpenTextFile("C:\BarcodeFTP\Glass_Per_Shift.txt", ForWriting)
objFile.Write strContents & vbCRLF & startDate & Chr(9) & shift & ":" & Chr(9) & counter & vbCRLF & lastRecord
objFile.Close
Set fso = Nothing

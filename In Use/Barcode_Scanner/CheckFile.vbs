' This 

' Sets some constants for reading and writing to files
Const ForReading = 1
Const ForWriting = 2	
Const ForAppending = 8

Dim objFSO, tempStr, strArray, strName, findTextFile, count, run, dateYear, myDateString, serialLength
Dim numEntries, numSkip, dupCheck, formatCheck, yearCheck, lineCheck, lenCheck, lineNumInt
Set objFSO = CreateObject("Scripting.FileSystemObject")	

' Path to the text file used to communicate with main application
txttempFile = "C:\Barcode_Scanner\TransferFile.txt"

' Sets the correct length for all serial numbers
serialLength = 14

' Gets data from the main application
If (objFSO.FileExists(txttempFile)) Then
	Set objFilesFile = objFSO.OpenTextFile(txttempFile, ForReading)
	If objFilesFile.AtEndOfStream Then
	Else
		txtDATA = objFilesFile.ReadLine
		txtFTPDATA = objFilesFile.ReadLine
		txtEntries = objFilesFile.ReadLine
		LineNum = objFilesFile.ReadLine
		destPath = objFilesFile.ReadLine
		linePath = objFilesFile.ReadLine
	End If
	objFilesFile.Close
End If

If (StrComp(LineNum,"A") = 0) Then
	lineNumInt = 1	
ElseIf (StrComp(LineNum,"B") = 0) Then
	lineNumInt = 42
Else
	lineNumInt = 4
End If

run = True

If objFSO.FolderExists(destPath) = False Then
	objFSO.CreateFolder (destPath)
End If

' Finds the current date
myDateString = Date()
dateYear = Right(myDateString,2)

' This File is used to determine how many lines have already been checked.
' Includes other data checking to ensure duplicate emails are not sent
If (objFSO.FileExists(txtEntries)) Then
	Set objEntriesFile = objFSO.OpenTextFile(txtEntries, ForReading)
	If objEntriesFile.AtEndOfStream Then
	Else
		numEntries = objEntriesFile.ReadLine
		numSkip = objEntriesFile.ReadLine
		dupCheck = objEntriesFile.ReadLine
		formatCheck = objEntriesFile.ReadLine
		yearCheck = objEntriesFile.ReadLine
		lineCheck = objEntriesFile.ReadLine
		lenCheck = objEntriesFile.ReadLine
	End If
	objEntriesFile.Close
End If

If (objFSO.FileExists(linePath & txtDATA)) Then
	If (objFSO.FileExists(txtFTPDATA)) Then
		objFSO.CopyFile txtFTPDATA, linePath, True
		objFSO.DeleteFile(txtFTPDATA)
														
	wscript.sleep 1000

	Set objFile = objFSO.OpenTextFile(linePath & txtDATA, ForReading)		

	' Keeps track of the number of entries read
	count = 0	
	' Checks various parameters as each line gets attended and reports any anomalies
	Do Until run = False
	
		' Will read from the file until it has reached the end.
		' Then it will update the file and exit the program.				
		If objFile.AtEndOfStream Then			
			run = False
			
		' Reads in a newline
		Else
			tempstrName = objFile.ReadLine						
			strArray = split(tempstrName,",")
			If Ubound(strArray) > 1 Then
				tempstrName = strArray(2)
			End If				
			
			If tempstrName <> "undefined" Then
				strName = tempstrName
			End If		
			
			count = count + 1

			If Not strName = "" Then				
				' Checks if this is a new or old entry
				If count > numEntries-0 Then						
					findTextFile = formatEntry(strName, destPath)
					' Calls the function that will check for duplicates
					strText = searchFile(destPath, strName, findTextFile, LineNum)
					
					' Used to check for at least 2 duplicates before a email is sent																			
					If dupCheck < 3 And strText <> "" Then
						strText = ""
						dupCheck = dupCheck + 1
					ElseIf strText = "" Then
						dupCheck = 0
					Else									
						dupCheck = 2
					End If
					
					' Checks to make sure the year in the serial number is correct										
					If Mid(strName, 2, 2) <> dateYear Then
						strText = strText & "Warning the year on this barcode file is incorrect: " & strName & Chr(13)
					End If
					If yearCheck <> 0 And strText <> "" Then
						strText = strText & ""
					ElseIf strText = "" Then
						yearCheck = 0
					Else									
						yearCheck = 1
					End If
							
					' Checks that the serial number has the correct length					
					If Len(strName) <> serialLength Then
						strText = strText & "This line has too many or too few characters in it: " & strName & Chr(13)
					End If
					If lenCheck <> 0 And strText <> "" Then
						strText = strText & ""
					ElseIf strText = "" Then
						lenCheck = 0
					Else									
						lenCheck = 1
					End If
					
					'Checks that the serial number is being printed on the correct line					
					If CInt(Mid(strName, 6, 1)) <> Cint(lineNumInt) Then
						strText = strText & "Problem this barcode label was meant for line: " & CInt(Mid(strName, 6, 1)) & ", not line: " & lineNumInt & Chr(13)
						'MsgBox strText
						'MsgBox strName & ", " & LineNum
						strText = ""
					End If
					If lineCheck <> 0 And strText <> "" Then
						strText = strText & ""
					ElseIf strText = "" Then
						lineCheck = 0
					Else									
						lineCheck = 1
					End If
					
					' Checks that the serial number has the correct fields
					If Mid(strName, 1, 1) <> "1" Or Not IsNumeric(Mid(strName, 5, 2)) Or Not IsNumeric(Mid(strName, 7, 3)) Then
						strText = strText & "Warning a part of this serial number is incorrectly formatted: " & strName & Chr(13)
					End If	
					If formatCheck <> 0 And strText <> "" Then
						strText = strText & ""
					ElseIf strText = "" Then
						formatCheck = 0
					Else									
						formatCheck = 1
					End If
													
				End If
				
			End If
						
			' If the serial number was undefined it means that the camera could not detect barcode
			' In this case a duplicate is not reported
			If tempstrName = "undefined" Then
				numSkip = numSkip + 1				
				strText = ""
			Else
				numSkip = 0
			End If	
			
			' This will ensure an email will only be sent once
			If numSkip > 2  And numSkip < 1000 Then
				strText = "Autoprinter is not working."
				numSkip = 1000
			End If
			
			' Sends an email
			If strText <> "" Then
				sendEmail strText, LineNum
			End If
			strText = ""
			
			' Updates the number of lines found in the text file
			If (objFSO.FileExists(txtEntries)) Then						
				If count > numEntries-0 Then
					Set objEntriesFile = objFSO.OpenTextFile(txtEntries, ForWriting)
					objEntriesFile.Write 0 & vbCrLf & numSkip & vbCrLf & dupCheck & vbCrLf & yearCheck & vbCrLf & formatCheck & vbCrLf & lineCheck & vbCrLf & lenCheck
					objEntriesFile.Close											
				End If					
			End If
			
		End If							
	Loop	
	objFile.Close
	
	'Set objFile = objFSO.OpenTextFile(linePath & txtDATA, ForWriting)
	'objFile.Write ""
	'objFile.Close
	
End If	
End If


' ----------------------------------------------------------------------------------------------------------------------------------------------------- '
' Function that returns the name of the text file
Private Function formatEntry(strName, destPath)
	Dim findTextFileMO, findTextFileYear, mystr, findLine, LineSelect

	findTextFileMO = Mid(strName, 8, 3)
	findTextFileYear = Mid(strName, 2, 2)
	findLine = Mid(strName, 6, 1)
	
	If findLine = 1 Then
		LineSelect = "A"
	ElseIf findLine = 2 Then
		LineSelect = "B"
	Else
		LineSelect = "D"
	End If
	
	mystr = "\Barcode_Data_" & findTextFileMO & "_" & findTextFileYear & "_" & LineSelect
					
	If(objFSO.FolderExists(destPath & "\20" & findTextFileYear)) = False Then
		objFSO.CreateFolder (destPath & "\20" & findTextFileYear)
	End If
	If objFSO.FileExists(destPath & "\20" & findTextFileYear & mystr) = False Then
		objFSO.CreateTextFile(destPath & "\20" & findTextFileYear & mystr)
	End If
	
	findTextFile = findTextFileYear & mystr
	
	formatEntry = findTextFile
End Function

' Function that checks for duplicate serial numbers, using the MO number to find the correct folder.
Private Function searchFile(destPath, strName, findTextFile, LineNum)
	Dim strText, strTempName, strArray
	strText = ""
	
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	' Opens the file corresponding to the entries MO and year
	If (objFSO.FileExists(destPath & "\20" & findTextFile)) Then
		Set objStoreFile = objFSO.OpenTextFile(destPath & "\20" & findTextFile, ForReading)
		' Searches through that text file to check for any duplicates
		Do Until objStoreFile.AtEndOfStream	
			strTempName = objStoreFile.ReadLine
			strArray = split(strTempName,",") 
			strTempName = strArray(0)
			if(strTempName = strName) Then
				strText = "This is a duplicate serial number: " & strName & Chr(13)				
			End If									
		Loop
		objStoreFile.Close
	End If
	
	' Writes all entries into the textfile
	If (objFSO.FileExists(destPath & "\20" & findTextFile)) Then
		Set objStoreFile = objFSO.OpenTextFile(destPath & "\20" & findTextFile, ForAppending)
		objStoreFile.WriteLine strName & ", " & Now & ", Line " & LineNum
		
		objStoreFile.Close
	End If

	searchFile = strText

End Function

' Function called to send an email
Function sendEmail(strText, LineNum)
	txtEmailList = "C:\Barcode_Scanner\Email_List.txt"
	
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	
	' Builds the email list from the text file
	Set objEmailFile = objFSO.OpenTextFile(txtEmailList, ForReading)
	Do While objEmailFile.AtEndOfStream = False
		emailList = emailList & objEmailFile.Readline & ";"
		count = count + 1	
	Loop
	objEmailFile.Close
	
	' Initializes email parameters
	Const fromEmail	= "GuelphProduction.Err@canadiansolar.com" 
	Const password	= "Sol@rprod99" 
	Dim emailObj, emailConfig 
	Set emailObj = CreateObject("CDO.Message") 
	emailObj.From = fromEmail
	emailObj.To = "zach.read@canadiansolar.com"
	'emailObj.To = emailList
	emailObj.Subject = "The Auto Printer has Failed on Line " & LineNum
	emailObj.TextBody = strText

	' Email configuration
	Set emailConfig = emailObj.Configuration 
	emailConfig.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "csica01ms01.csisolar.com"
	emailConfig.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
	emailConfig.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
	emailConfig.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = cdoBasic 
	emailConfig.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true
	emailConfig.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = fromEmail 
	emailConfig.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = password
	emailConfig.Fields.Update
	'msgbox(strText);
	emailObj.Send
End Function
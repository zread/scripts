' This script is used to find the quantity of panels per MO
Const ForReading = 1
Const ForWriting = 2	
Const ForAppending = 8

Dim strContents, fso, strMO, strInfo, strTxt

Set fso = CreateObject("Scripting.FileSystemObject")

findYear = split(Date(), "/")

strInfo = Date & Chr(13) & "MO," & Time & Chr(13)
strTxt =  Date() & " " & Time & vbCRLF & "MO" & Chr(9) & "Panels" & vbCRLF
For Each File In fso.GetFolder("C:\BarcodeFTP\Data_Files\" & findYear(2)).Files
	Set theFile = fso.OpenTextFile(File.Path, 1, True)
	theFile.ReadAll	
	strMO = Mid(File.Path, 44, 3)	
	strInfo = strInfo & strMO & "," & theFile.Line-1 & Chr(13)
	strTxt = strTXT & strMO & ":" & Chr(9) &  theFile.Line-1 & vbCRLF
	theFile.Close
Next

Set objFile = fso.OpenTextFile("C:\BarcodeFTP\Glass_Per_MO.csv", ForReading)
strContents = objFile.ReadAll
objFile.Close

Set objFile = fso.OpenTextFile("C:\BarcodeFTP\Glass_Per_MO.csv", ForWriting)
objFile.Write strInfo & Chr(13) & Chr(13) & strContents
objFile.Close

Set objFile = fso.OpenTextFile("C:\BarcodeFTP\Glass_Per_MO.txt", ForWriting)
objFile.Write strTxt
objFile.Close

Set objFile = fso.OpenTextFile("C:\BarcodeFTP\Current_MO.txt", ForReading)
FirstLine = objFile.ReadLine


Do While objFile.AtEndofStream = False
	newMO = objFile.ReadLine
	splitMO = split(newMO, Chr(9))		
	If (fso.FileExists("C:\BarcodeFTP\Data_Files\" & findYear(2) & "\Barcode_Data_" & splitMO(0) & "_" & findYear(2)-2000)) Then		
		Set theFile = fso.OpenTextFile("C:\BarcodeFTP\Data_Files\" & findYear(2) & "\Barcode_Data_" & splitMO(0) & "_" & findYear(2)-2000, 1, True)
		theFile.ReadAll	
		splitMO(1) = theFile.Line-1
	End If
	If UBound(splitMO) < 5 Then
		ReDim Preserve splitMO(5)
		splitMO(5) = "No"
	End If
	If splitMO(5) <> "No" And splitMO(5) <> "Yes" Then
		splitMO(5) = "No"
	End IF	
	If splitMO(1) + 76 > splitMO(3) + 1 And splitMO(5) = "No" Then
		splitMO(5) = "Yes"
		sendEmail("MO " & splitMO(0) & " has printed " & splitMO(1) & " of the " & splitMO(3) & " serial numbers.")		
	End If
	
	FirstLine = FirstLine & vbCRLF & splitMo(0) & Chr(9) & splitMo(1) & Chr(9) & splitMo(2) & Chr(9) & splitMo(3) & Chr(9) & splitMo(4) & Chr(9) & splitMo(5)
Loop

objFile.Close

Set objFile = fso.OpenTextFile("C:\BarcodeFTP\Current_MO.txt", ForWriting)
objFile.Write FirstLine
objFile.Close

Set fso = Nothing

Function sendEmail(strText)
	Const ForReading = 1	
	txtEmailList = "C:\Barcode_Scanner\MO_Email_List.txt"
		
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	
	Set objEmailFile = objFSO.OpenTextFile(txtEmailList, ForReading)
	Do While objEmailFile.AtEndOfStream = False
		emailList = emailList & objEmailFile.Readline & ";"
		count = count + 1	
	Loop
	objEmailFile.Close
	
	Const fromEmail	= "GuelphProduction.Err@canadiansolar.com" 
	Const password	= "Sol@rprod99" 
	Dim emailObj, emailConfig 
	Set emailObj = CreateObject("CDO.Message") 
	emailObj.From = fromEmail
	'emailObj.To = "Zach.Read@canadiansolar.com"
	emailObj.To = emailList
	emailObj.Subject = "MO Nearing Completion" 
	emailObj.TextBody = strText

	Set emailConfig = emailObj.Configuration 
	emailConfig.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "csica01ms01.csisolar.com"
	emailConfig.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
	emailConfig.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
	emailConfig.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = cdoBasic 
	emailConfig.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = true
	emailConfig.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = fromEmail 
	emailConfig.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = password
	emailConfig.Fields.Update
		
	emailObj.Send
End Function
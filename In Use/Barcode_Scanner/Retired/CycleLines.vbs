Set line = WScript.Arguments

Dim check

MSHTA = "mshta.exe"
sHTAA = "C:\Barcode_Scanner\Line_A\Barcode_Scanner_Line_A.hta"
sHTAB = "C:\Barcode_Scanner\Line_B\Barcode_Scanner_Line_B.hta"

check = false

endProcess MSHTA, sHTAA
endProcess MSHTA, sHTAB
WScript.Sleep 500
CreateObject("WScript.Shell").Run("C:\Barcode_Scanner\Line_A\Barcode_Scanner_Line_A.hta")
While ProcessRunning(MSHTA, sHTAA) = vbTrue
	WScript.Sleep 5000
Wend
CreateObject("WScript.Shell").Run("C:\Barcode_Scanner\Line_B\Barcode_Scanner_Line_B.hta")

' Checks to see if the program is in the process list and then terminates it
Function endProcess(sProcess, sParam)
  set oWMI = GetObject( _
    "winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
  set cProcs = oWMI.ExecQuery( _
    "select * from Win32_Process where Name = '" & sProcess & "'")

  bFound = vbFalse
  for each oProc in cProcs
    if (InStr(oProc.CommandLine, sParam) > 0) then
		oProc.Terminate
    end if
  next
  'MsgBox "Killed"
End function

' Checks to see if the program is in the process list
Function ProcessRunning(sProcess, sParam)
  set oWMI = GetObject( _
    "winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
  set cProcs = oWMI.ExecQuery( _
    "select * from Win32_Process where Name = '" & sProcess & "'")

  bFound = vbFalse
  for each oProc in cProcs
    if (InStr(oProc.CommandLine, sParam) > 0) then
      bFound = vbTrue      
    end if
  next

  ProcessRunning = bFound
End function
Const ForReading = 1
Const ForWriting = 2	
Const ForAppending = 8

' Where the mo files will be stored
destPath = "C:\Barcode_Scanner\Data_Files"
' The current serial number is stored here
txtTempFile = "C:\Barcode_Scanner\Line_A\TempFile.txt"
'Used to track the number of duplicates
txtDuplicates = "C:\Barcode_Scanner\Line_A\Barcode_Duplicates.txt"

Dim strName, strText

strText = ""

' This has the path to the textfile that this serial number should be placed in
findTextFile = WScript.Arguments(0)

Set objFSO = CreateObject("Scripting.FileSystemObject")

' Gets the newest data entry from the temporary text file
If (objFSO.FileExists(txtTempFile)) Then
	Set objTempFile = objFSO.OpenTextFile(txtTempFile, ForReading)
	If Not objTempFile.AtEndofStream Then
		strName = objTempFile.ReadLine
	End If
	objTempFile.Close
End If
	
' Opens the file coreesponding to the entries MO and year
If (objFSO.FileExists(destPath & "\20" & findTextFile)) Then
	Set objStoreFile = objFSO.OpenTextFile(destPath & "\20" & findTextFile, ForReading)
	' Searches through that text file to check for any duplicates
	Do Until objStoreFile.AtEndOfStream	
		strTempName = objStoreFile.ReadLine
		strArray = split(strTempName,",") 
		strTempName = strArray(0)
		if(strTempName = strName) Then
			strText = "This is a duplicate serial number: " & strName & Chr(13)
			Set objDuplicatesFile = objFSO.OpenTextFile(txtDuplicates, ForAppending)	
			objDuplicatesFile.Write strText & Chr(13)						
			objDuplicatesFile.Close
			objStoreFile.Close
			
			Set objTempFile = objFSO.OpenTextFile(txtTempFile, ForWriting)
			objTempFile.WriteLine strText
			objTempFile.Close		
			Exit Do
		End If									
	Loop
	objStoreFile.Close
End If
	
If (strText = "") Then
	Set objTempFile = objFSO.OpenTextFile(txtTempFile, ForWriting)
	objTempFile.Write ""
	objTempFile.Close
End If
	
' Writes the non duplicate entry into the textfile
If (objFSO.FileExists(destPath & "\20" & findTextFile)) Then
	Set objStoreFile = objFSO.OpenTextFile(destPath & "\20" & findTextFile, ForAppending)
	objStoreFile.WriteLine strName & ", " & Now & ", Line A"
	objStoreFile.Close
End If

WScript.Quit

If Not ProcessRunning("wscript.exe", "C:\Barcode_Scanner\CheckMonitor2.vbs") Then	
	CreateObject("WScript.Shell").Run "C:\Barcode_Scanner\CheckMonitor2.vbs"	
End if
Wscript.Quit

' Checks to see if the program is in the process list
Function ProcessRunning(sProcess, sParam)
  set oWMI = GetObject( _
    "winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
  set cProcs = oWMI.ExecQuery( _
    "select * from Win32_Process where Name = '" & sProcess & "'")

  bFound = vbFalse
  for each oProc in cProcs
    if (InStr(oProc.CommandLine, sParam) > 0) then
      bFound = vbTrue      
    end if
  next

  ProcessRunning = bFound
End function
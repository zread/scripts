' Checks to makes sure the Packaging Program Monitor is running every 40 seconds
MSHTA = "mshta.exe"
sHTAA = "C:\Barcode_Scanner\Line_A\Barcode_Scanner_Line_A.hta"
sHTAB = "C:\Barcode_Scanner\Line_B\Barcode_Scanner_Line_B.hta"

' set the forty second delay
secs = 40000

'Continously run
While(1)
	bRunning = vbTrue
	
	' checks to see if the program is running, if its not restarts it
	do while bRunning
	  if ProcessRunning(MSHTA, sHTAA) Or ProcessRunning(MSHTA, sHTAB) Or ProcessRunning("wscript.exe", "C:\Barcode_Scanner\CycleLine.vbs")  then   
		WScript.Sleep secs		
	  else
		bRunning = vbFalse
	  end if
	loop

	WScript.Sleep secs
	
	If ProcessRunning(MSHTA, sHTAA) Or ProcessRunning(MSHTA, sHTAB) Or ProcessRunning("wscript.exe", "C:\Barcode_Scanner\CycleLine.vbs")  then
	Else
		CreateObject("WScript.Shell").Run(sHTAA)
	End If
Wend
'---'

' Checks to see if the program is in the process list
Function ProcessRunning(sProcess, sParam)
  set oWMI = GetObject( _
    "winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
  set cProcs = oWMI.ExecQuery( _
    "select * from Win32_Process where Name = '" & sProcess & "'")

  bFound = vbFalse
  for each oProc in cProcs
    if (InStr(oProc.CommandLine, sParam) > 0) then
      bFound = vbTrue      
    end if
  next

  ProcessRunning = bFound
End function
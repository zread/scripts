Set line = WScript.Arguments

Dim check

MSHTA = "mshta.exe"
sHTAA = "C:\Barcode_Scanner\Monitor_Lines.hta"

check = false

endProcess MSHTA, sHTAA
WScript.Sleep 500
CreateObject("WScript.Shell").Run("C:\Barcode_Scanner\Monitor_Lines.hta")
While ProcessRunning(MSHTA, sHTAA) = vbTrue
	WScript.Sleep 5000
Wend

' Checks to see if the program is in the process list and then terminates it
Function endProcess(sProcess, sParam)
  set oWMI = GetObject( _
    "winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
  set cProcs = oWMI.ExecQuery( _
    "select * from Win32_Process where Name = '" & sProcess & "'")

  bFound = vbFalse
  for each oProc in cProcs
    if (InStr(oProc.CommandLine, sParam) > 0) then
		oProc.Terminate
    end if
  next
End function

' Checks to see if the program is in the process list
Function ProcessRunning(sProcess, sParam)
  set oWMI = GetObject( _
    "winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
  set cProcs = oWMI.ExecQuery( _
    "select * from Win32_Process where Name = '" & sProcess & "'")

  bFound = vbFalse
  for each oProc in cProcs
    if (InStr(oProc.CommandLine, sParam) > 0) then
      bFound = vbTrue      
    end if
  next

  ProcessRunning = bFound
End function
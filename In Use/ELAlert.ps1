﻿$threshold = 4
$SQLServer = "CA01S0015"
$user = "reader"
$password = "12345"
$SQLDBName = "CSILabelPrintDB"
$TodayDate = Get-Date
$SqlQuery = 
"Select Line, SN From QualityJudge Where Remark = 'Auto KUKA EL Reject' and Timestamp > DATEADD(mi,-10,GetDate())"

$smtpServer = "csica01ms01.csisolar.com"
$msg = new-object Net.Mail.MailMessage
$smtp = new-object Net.Mail.SmtpClient($smtpServer)
$msg.From = "GuelphProduction.Err@canadiansolar.com"
#$msg.ReplyTo = "replyto@xxxx.com"
$msg.To.Add("Zach.Read@canadiansolar.com")
$msg.To.Add("Nasim.Zadeh@canadiansolar.com")
$msg.To.Add("Quality_Dept@canadiansolar.com")
$msg.To.Add("ProductionSupervisors@canadiansolar.com")

$SqlConnection = New-Object System.Data.SqlClient.SqlConnection
$SqlConnection.ConnectionString = "Server=$SQLServer;uid=$user; pwd=$password;Database=$SQLDBName;"
 
$SqlCmd = New-Object System.Data.SqlClient.SqlCommand
$SqlCmd.CommandText = $SqlQuery
$SqlCmd.Connection = $SqlConnection
 
$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
$SqlAdapter.SelectCommand = $SqlCmd
 
$DataSet = New-Object System.Data.DataSet
$SqlAdapter.Fill($DataSet)
 
$SqlConnection.Close()

$Line = ""
$rejects = ""

$strMessage = "The following serial numbers have failed KUKA EL:`n" 
$messageNeeded = 0 

for($i=0;$i -le $DataSet.Tables[0].Rows.Count - 1 ;$i++)
{ 
    $strMessage = "" + $strMessage + "Line " + $DataSet.Tables[0].Rows[$i][0]  + ":   " + $DataSet.Tables[0].Rows[$i][1] + "`n"
    $messageNeeded = 1
}

if($messageNeeded -eq 1)
{    
    $msg.body = "Hello, this is an automated message. `n `n" + $strMessage
	$msg.subject = "The Following Are KUKA EL Rejects."	
    $smtp.Send($msg)
}

Remove-Variable DataSet

function IsNumeric($Value) {
    return $Value -match "^[\d\.]+$"
}

$SqlQuery = 
"Select SN From QualityJudge Where Timestamp > DATEADD(mi,-10,GetDate())"

$Line = ""
$rejects = ""
$strMessage = "The following serial numbers were A- with an invalid serial number:`n" 
$messageNeeded = 0 

$SqlCmd.CommandText = $SqlQuery
$SqlCmd.Connection = $SqlConnection 
$SqlAdapter.SelectCommand = $SqlCmd
$DataSet = New-Object System.Data.DataSet
$SqlAdapter.Fill($DataSet) 
$SqlConnection.Close()

for($i=0;$i -le $DataSet.Tables[0].Rows.Count - 1 ;$i++)
{ 
    Write-Host $DataSet.Tables[0].Rows[$i][0]
    $test = IsNumeric($DataSet.Tables[0].Rows[$i][0])
    Write-Host $test
    if($test -eq $False)
    {
    	$strMessage = "" + $strMessage + "SN " + $DataSet.Tables[0].Rows[$i][0]
        $messageNeeded = 1
    }
}

if($messageNeeded -eq 1)
{    
    $msg.body = "Hello, this is an automated message. `n `n" + $strMessage
	$msg.subject = "The Following SN Was Recorded Incorrectly."	
    $smtp.Send($msg)
}

Start-Sleep -s 2
Remove-Variable strMessage
Remove-Variable Line
Remove-Variable rejects



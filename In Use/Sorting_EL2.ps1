﻿#$Src_dir = "\\ca01a9007\ELTester\LineA\File\Bilder"
#$Src_dir = "\\ca01a9007\eltester\Requested Images"
$Src_dir = "\\ca01a9004\EL_Tester\UnsortedImages"
#$Src_dir =  "\\ca01a9004\EL_Tester\Bilder"
$Dest_Dir = "\\ca01a9004\EL_Tester"
$log_file_dir = "C:\Users\Zach.Read\Desktop\log.txt"
$counter = 0
write $src_dir
Get-ChildItem $src_dir -Filter 17*.jpg -recurse | 

Foreach-Object {
$counter = 0
    #write $_.LastWriteTime
    if($_.LastWriteTime -lt (get-date).AddHours(-12)){
        if($_.FullName -like '*Rejected*'){
            $pathFull = $Dest_Dir + "\Images" + "\" + $_.Name.Substring(0,2) + "\" + $_.Name.Substring(4,2) + "\" + $_.Name.Substring(6,3) + "\" + "Rejected"
        }
        else{
            $pathFull = $Dest_Dir + "\Images" + "\" + $_.Name.Substring(0,2) + "\" + $_.Name.Substring(4,2) + "\" + $_.Name.Substring(6,3) + "\" + "Accepted"
        }

        if(!(Test-Path $pathFull))
        {
            New-Item -Path $pathFull -ItemType Directory
        }
    
        while([System.IO.File]::Exists("$($pathFull)\1$($_.basename)_$($counter)$($_.extension)"))
		{			
			$counter = $counter + 1
		}
		#write "$($pathFull)\3$($_.basename)_$($counter)$($_.extension)"
        #Copy-Item $_.FullName -Destination "$($pathFull)\3$($_.Name)" -PassThru 
		Copy-Item $_.FullName -Destination "$($pathFull)\1$($_.basename)_$($counter)$($_.extension)" -PassThru
		
		#write $_.FullName
	
        if($?){
	        Remove-Item $_.FullName
            #write "Delete"
        }
    }
}

Get-ChildItem $src_dir -Filter 17*.tif -recurse | 

Foreach-Object {
    write $_.CreationTime
    if($_.CreationTime -lt (get-date).AddHours(-12)){
        if($_.FullName -like '*Rejected*'){
            $pathFull = $Dest_Dir + "\Images" + "\" + $_.Name.Substring(0,2) + "\" + $_.Name.Substring(4,2) + "\" + $_.Name.Substring(6,3) + "\" + "Rejected"
        }
        else{
            $pathFull = $Dest_Dir + "\Images" + "\" + $_.Name.Substring(0,2) + "\" + $_.Name.Substring(4,2) + "\" + $_.Name.Substring(6,3) + "\" + "Accepted"
        }

        if(!(Test-Path $pathFull))
        {
            New-Item -Path $pathFull -ItemType Directory
        }
    
        Copy-Item $_.FullName -Destination "$($pathFull)\1$($_.Name)" -PassThru  
    
        if($?){
	        Remove-Item $_.FullName
            write "Delete"
        }
    }
}

write "Done"
$SQLServer = "CA01S0015"
$user = "iDashboards"
$password = "solardash"
$SQLDBName = "CSILabelPrintDB"
$RejectFolder = "\\ca01a9004\EL_Tester\EOL_EL_Tester\RejectedA-"
$NewFolder = "\\ca01a9004\EL_Tester\EOL_EL_Tester\Rejected"


$files = Get-ChildItem $RejectFolder -Filter *.jpg

for ($i=0; $i -lt $files.Count; $i++)
{ 	
	$SN = $files[$i].BaseName
    Write-Output $SN

    if($SN.length -gt 12){
        $num = 0
        $SqlQuery = "Insert into QualityJudge (SN, ModuleClass, timestamp, operator, defectcode, remark)
    	Values ('"+ $SN.Substring(0,14) +"', 'A-', getdate(), 'EL3', '', 'EL3 Reject')"; 

        $SqlConnection = New-Object System.Data.SqlClient.SqlConnection       	
    	$SqlConnection.ConnectionString = "Server = $SQLServer; Database = $SQLDBName; User ID = $user; Password = $password;"
        $SqlConnection.Open()
        $SqlCmd = $SqlConnection.CreateCommand()
    	$SqlCmd.CommandText = $SqlQuery
    	$SqlCmd.Connection = $SqlConnection		 
    	$SqlCmd.ExecuteNonQuery()   
    	$SqlConnection.Close()
        
        $nextName = Join-Path -Path $NewFolder -ChildPath $files[$i].name        
        while(Test-Path -Path $nextName)
        {
           $nextName = Join-Path $NewFolder ($files[$i].BaseName + "_$num" + $files[$i].Extension)    
           $num+=1   
        }
        $files[$i] | Move-Item -Destination $nextName
    }
}


$SQLServer = "CA01S0015"
$user = "iDashboards"
$password = "solardash"
$SQLDBName = "CSILabelPrintDB"
$RejectFolder = "\\ca01a9004\EL_Tester\EOL_EL_Tester\Rejected"
$AcceptedFolder = "\\ca01a9004\EL_Tester\EOL_EL_Tester\Accepted"
$Date = (Get-Date -Format "yyyy-MM-dd HH:mm:ss")

$SqlQuery = "SELECT SN FROM EOLELImageChecker Where ImageFound = 0 and ISNULL(Checked,0) = 0 and DateAdd(mi, 12, RecordTime) < '" + $Date + "'"

$SqlConnection = New-Object System.Data.SqlClient.SqlConnection
$SqlConnection.ConnectionString = "Server=$SQLServer;Database=$SQLDBName;User ID=$user;Password=$password;"

$SqlCmd = New-Object System.Data.SqlClient.SqlCommand
$SqlCmd.CommandText = $SqlQuery
$SqlCmd.Connection = $SqlConnection 
$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
$SqlAdapter.SelectCommand = $SqlCmd
$DataSet = New-Object System.Data.DataSet
$SqlAdapter.Fill($DataSet) 
$SqlConnection.Close()

Foreach ($record in $Dataset.Tables[0].Rows){
    $path = ""
    Get-ChildItem -Path $AcceptedFolder -Filter ($record[0] + "*.jpg") | ForEach-Object {
        write-Host $_.FullName
        $path = $_.FullName
    }
    if(![System.IO.File]::Exists($path)){     
        Get-ChildItem -Path $RejectFolder -Filter ($record[0] + "*.jpg") | ForEach-Object {
            write-Host $_.FullName
            $path = $_.FullName
        }
        if(![System.IO.File]::Exists($path)){
            $SqlQuery = "Insert into QualityJudge (SN, ModuleClass, timestamp, operator, defectcode, remark)
    	    Values ('"+ $record[0] +"', 'A-', getdate(), 'EL3', '', 'No EL3 Image Found') 
            Update EOLELImageChecker Set Checked = 1 where SN = '" + $record[0] + "'";
            #$SqlQuery = "Update EOLELImageChecker Set Checked = 1 where SN = '" + $record[0] + "'";
        }        
        else{            
            $SqlQuery = "Update EOLELImageChecker Set ImageFound = 1, Checked = 1 where SN = '" + $record[0] + "'"
        }
    }    
    else{ 
        $SqlQuery = "Update EOLELImageChecker Set ImageFound = 1, Checked = 1 where SN = '" + $record[0] + "'"        
    }
    
    $SqlConnection.Open()
    $SqlCmd = $SqlConnection.CreateCommand()
	$SqlCmd.CommandText = $SqlQuery
	$SqlCmd.Connection = $SqlConnection		 
	$SqlCmd.ExecuteNonQuery()   
	$SqlConnection.Close()
}
$Src_org_good =  "C:\CYCLEDATA\FinalImages1\*jpg"
$Src_org_bad =  "C:\CYCLEDATA\FinalFailedImages1\*jpg"

$Dest_local_Dir_good = "Z:\EL_Tester\EOL_EL_Tester\Accepted"
$Dest_local_Dir_bad = "Z:\EL_Tester\EOL_EL_Tester\Rejected"

#$Src_org_good = "C:\Users\Zach.Read\Desktop\Pic Test\Local"
#$Dest_local_Dir_good = "C:\Users\Zach.Read\Desktop\Pic Test\Good"


Get-ChildItem -Path $Src_org_good -Filter *.jpg -Recurse | ForEach-Object {
    $num = 0
    if($_.name.length -gt 12){
        $nextName = Join-Path -Path $Dest_local_Dir_good -ChildPath $_.name

        while(Test-Path -Path $nextName)
        {
           $nextName = Join-Path $Dest_local_Dir_good ($_.BaseName + "_$num" + $_.Extension)    
           $num+=1   
        }

        $_ | Move-Item -Destination $nextName
    }
}

Get-ChildItem -Path $Src_org_bad -Filter *.jpg -Recurse | ForEach-Object {
    $num = 0
    if($_.name.length -gt 12){
        $nextName = Join-Path -Path $Dest_local_Dir_bad -ChildPath $_.name

        while(Test-Path -Path $nextName)
        {
           $nextName = Join-Path $Dest_local_Dir_bad ($_.BaseName + "_$num" + $_.Extension)    
           $num+=1   
        }

        $_ | Move-Item -Destination $nextName
        
        #Move-Item $Src_org_good $Dest_local_Dir_good
        #Move-Item $Src_org_bad $Dest_local_Dir_bad
    }
}